# https://docs.python.org/3.6/library/socketserver.html#module-SocketServer
# https://docs.python.org/3.6/library/asyncio-protocol.html#udp-echo-client-protocol
import socket
import msgpack
import threading
import time

import sv_robo.api
# import sv_robo.message
# import sv_robo.constants

socket.setdefaulttimeout(5.0)

PAULI9_IP = "10.10.12.62"
UDP_PORT_NUMBER = 8899  # We should give the port numbers and the IP address in both Client and the Server as must be same


def listen():
    UDP_IP_ADDRESS = "10.10.12.62"  # socket.gethostname()
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind((UDP_IP_ADDRESS, UDP_PORT_NUMBER))

    while True:
        data, address = server_socket.recvfrom(100)
        print(f"Message: {data}, address: {address}")


thread = threading.Thread(target=listen)
thread.start()

# export PYTHONPATH=/home/sv/bags/sv-robo-api:$PYTHONPATH
# export PYTHONPATH=/home/keivan/bags/sv-robo-api:$PYTHONPATH
if True:
    UDP_IP_ADDRESS = PAULI9_IP

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    for i in range(4):
        msg = sv_robo.api.create_request_axis_update_message(i)
        print(f"sending {msg}")
        client_socket.sendto(msg, (UDP_IP_ADDRESS, UDP_PORT_NUMBER))
        time.sleep(1.0)

    thread.join()
