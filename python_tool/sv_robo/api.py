import msgpack
import enum

import sv_robo
import sv_robo.constants
import sv_robo.message


def create_message(message_type: sv_robo.constants.HeaderMessageType) -> bytes:
    if message_type == sv_robo.constants.HeaderMessageType.DISCOVER:
        return create_discover_message()
    elif message_type == sv_robo.constants.HeaderMessageType.UPDATE_REF:
        return create_request_axis_update_message(0)
    else:
        raise NotImplementedError("Not implemented.")


def create_discover_message() -> bytes:
    msg = sv_robo.message.Message()
    msg.header.type = sv_robo.constants.HeaderMessageType.DISCOVER.value
    packed = msgpack.packb(msg, default=sv_robo.message.encode)
    return packed


def create_request_axis_update_message(counter: int) -> bytes:
    msg = sv_robo.message.Message()
    msg.header.number = counter
    msg.header.type = sv_robo.constants.HeaderMessageType.UPDATE_REF.value
    # msg.payload.data[sv_robo.constants.Axis.GLOBAL.value] = None
    msg.payload.data[sv_robo.constants.Axis.PAN.value] = {
        sv_robo.constants.PayloadIdentifier.ANGULAR_VELOCITY.value: 0.0
    }
    msg.payload.data[sv_robo.constants.Axis.TILT.value] = {
        sv_robo.constants.PayloadIdentifier.ANGULAR_VELOCITY.value: 0.0
    }
    packed = msgpack.packb(msg, default=sv_robo.message.encode)
    return packed
