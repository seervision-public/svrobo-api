import msgpack
from typing import List


class Header:
    def __init__(self) -> None:
        self.crc = 0
        self.number = 0
        self.type = 0

    def __eq__(self, other: "Header") -> bool:
        return (self.number == other.number) and (self.type == other.type)

    def __str__(self) -> str:
        return f"{{crc: {self.crc}, number: {self.number}, type: {self.type} }}"


def encode(obj) -> List[int]:
    if isinstance(obj, Header):
        return [obj.crc, obj.number, obj.type]
    raise ValueError(f"Wrong input for object {obj}")


def decode(packed_list: list) -> Header:
    if isinstance(packed_list, list) and len(packed_list) == 2:
        obj = Header()
        obj.number = packed_list[0]
        obj.type = packed_list[1]
        return obj
    raise ValueError(f"Wrong input for object {packed_list}")
