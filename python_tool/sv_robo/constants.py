import enum


class HeaderMessageType(enum.Enum):
    UPDATE_REF = 0
    SET_PARAM = 1
    GET_PARAM = 2
    REQ_STATE_ACTION = 3
    DISCOVER = 4
    SET_NET_DATA = 5


class Axis(enum.Enum):
    GLOBAL = 0
    PAN = 1
    TILT = 2
    ROLL = 3
    ZOOM = 4
    FOCUS = 5
    IRIS = 6
    AXIS_X = 7
    AXIS_Y = 8
    AXIS_Z = 9
    RANGE = 10


class PayloadIdentifier(enum.Enum):
    ANY = 0
    POSITION = 1
    VELOCITY = 2
    ACCELERATION = 3
    UNIT_POSITION = 4
    UNIT_VELOCITY = 5
    UNIT_ACCELERATION = 6
    ANGULAR_POSITION = 7
    ANGULAR_VELOCITY = 8
    ANGULAR_ACCELERATION = 9
    CURRENT = 10
    TORQUE = 11
    TIMESTAMP = 12
