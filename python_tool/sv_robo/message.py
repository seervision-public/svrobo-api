import msgpack
from typing import Optional

import sv_robo.header
import sv_robo.payload
import sv_robo.constants


class Message:
    def __init__(
        self,
        header: sv_robo.header.Header = sv_robo.header.Header(),
        payload: sv_robo.payload.Payload = sv_robo.payload.Payload()
    ) -> None:
        self.header = header
        self.payload = payload

    def __eq__(self, other: "Message") -> bool:
        return (self.header == other.header) and (self.payload == other.payload)

    def __str__(self) -> str:
        return f"{{header: {self.header},\n payload: {self.payload} }}"


def encode(obj):
    if isinstance(obj, Message):
        return [sv_robo.header.encode(obj.header), sv_robo.payload.encode(obj.payload)]
    raise ValueError(f"Wrong input for {obj}")


def decode(packed_obj: list) -> Message:
    if isinstance(packed_obj, list):
        if len(packed_obj) == 2:
            header = sv_robo.header.decode(packed_obj[0])
            payload = sv_robo.payload.decode(header.type, packed_obj[1])
            return Message(header, payload)
    raise ValueError(f"Wrong input for {packed_obj}")
