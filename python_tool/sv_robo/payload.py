import sv_robo.constants

import collections
from abc import ABC, abstractmethod


class GenericPayload(ABC):
    @abstractmethod
    def __eq__(self, other: "GenericPayload") -> bool:
        return False


class Payload(GenericPayload):
    def __init__(self) -> None:
        self.data = {}

    def __eq__(self, other: "Payload") -> bool:
        return (self.data == other.data)

    def __str__(self) -> str:
        return f"{self.data}"


class NetworkPayload(GenericPayload):
    Api = collections.namedtuple("Api", ["version_major", "version_minor", "type"])
    NetworkInfo = collections.namedtuple("NetworkInfo", ["ip", "mask", "mac"])

    def __init__(self) -> None:
        self.api = NetworkPayload.Api(0, 0, 0)
        self.data = NetworkPayload.NetworkInfo("", "", "")

    def __eq__(self, other: "NetworkPayload") -> bool:
        return (self.data == other.data) and (self.api == other.api)

    def __str__(self) -> str:
        return f"api: {self.api}, network info: {self.data}"


def encode(obj: GenericPayload) -> list:

    if isinstance(obj, Payload):
        return obj.data

    if isinstance(obj, NetworkPayload):
        return [obj.api.version_major, obj.api.version_minor, obj.api.type, [obj.data.ip, obj.data.mask, obj.data.mac]]

    return []


def decode(header_type: int, payload: dict) -> GenericPayload:
    # example {
    #   1: { 8: 0.1 },
    #   6: { 1: 1.2 },
    # }
    if header_type == sv_robo.constants.HeaderMessageType.UPDATE_REF.value:
        out = Payload()
        out.data = payload
        return out
    if header_type == sv_robo.constants.HeaderMessageType.DISCOVER.value:
        out = NetworkPayload()
        out.api = NetworkPayload.Api(payload[0], payload[1], payload[2])
        out.data = NetworkPayload.NetworkInfo(payload[3][0], payload[3][1], payload[3][2])

        return out

    return Payload()
