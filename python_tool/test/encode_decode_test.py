import sys

import unittest

import msgpack
import sv_robo
import sv_robo.header
import sv_robo.payload
import sv_robo.message
import sv_robo.constants


class EncodeDecodeTest(unittest.TestCase):
    def test_header(self):
        header = sv_robo.header.Header()
        header.number = 10
        header.type = 2
        packed = msgpack.packb(header, default=sv_robo.header.encode)
        unpacked = msgpack.unpackb(packed)
        header_decoded = sv_robo.header.decode(unpacked)
        self.assertEqual(header, header_decoded)

    def test_message(self):

        header = sv_robo.header.Header()
        header.number = 10
        header.type = 2

        payload = sv_robo.payload.Payload()
        m = sv_robo.message.Message(header=header, payload=payload)

        packed = msgpack.packb(m, default=sv_robo.message.encode)

        unpacked = msgpack.unpackb(packed, strict_map_key=False)
        msg = sv_robo.message.decode(unpacked)
        self.assertEqual(m, msg)

    def test_update_ref(self):
        def update_reference() -> sv_robo.message.Message:
            message = sv_robo.message.Message()
            message.header.type = sv_robo.constants.HeaderMessageType.UPDATE_REF.value
            message.payload.data[sv_robo.constants.Axis.PAN.value] = {
                sv_robo.constants.PayloadIdentifier.ANGULAR_VELOCITY.value: 0.1
            }
            message.payload.data[sv_robo.constants.Axis.AXIS_X.value] = {
                sv_robo.constants.PayloadIdentifier.POSITION.value: 1.0
            }
            message.payload.data[sv_robo.constants.Axis.ZOOM.value] = {
                sv_robo.constants.PayloadIdentifier.UNIT_POSITION.value: 0.7
            }

            return message

        message = update_reference()
        packed = msgpack.packb(message, default=sv_robo.message.encode)
        unpacked = msgpack.unpackb(packed, strict_map_key=False)
        msg = sv_robo.message.decode(unpacked)
        self.assertEqual(message, msg)

    def test_network_payload(self):
        msg = sv_robo.payload.NetworkPayload()
        msg.data = sv_robo.payload.NetworkPayload.NetworkInfo(ip="192.168.0.1", mask="255.255.255.0", mac="FE045AB")
        msg.api = sv_robo.payload.NetworkPayload.Api(version_major=1, version_minor=3, type=1)

        packed = msgpack.packb(msg, default=sv_robo.payload.encode)

        unpacked = msgpack.unpackb(packed, strict_map_key=False)
        msg_decoded = sv_robo.payload.decode(sv_robo.constants.HeaderMessageType.DISCOVER.value, unpacked)
        self.assertEqual(msg, msg_decoded)


if __name__ == '__main__':
    unittest.main()
