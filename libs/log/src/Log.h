#pragma once

#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE

// These include are on purpose listed here, so that we only have to include Log.h
#include <spdlog/sinks/basic_file_sink.h>
#include <iostream>
#include <map>
#include <numeric>
#include <variant>
#include "spdlog/async.h"
#include "spdlog/fmt/ostr.h"
#include "spdlog/spdlog.h"

namespace sv::log {

using Level = spdlog::level::level_enum;

using LogValue = std::variant<int,
                              unsigned int,
                              long int,
                              unsigned long int,
                              long long int,
                              unsigned long long int,
                              float,
                              double,
                              std::string,
                              bool,
                              std::vector<uint8_t>,
                              std::vector<char>>;

void log(const std::string& fileName,
         int lineNumber,
         Level level,
         const std::string& message,
         std::map<std::string, LogValue>&& values = {});

void logThrottled(const std::string& fileName,
                  int lineNumber,
                  Level level,
                  double period,
                  const std::string& message,
                  std::map<std::string, LogValue>&& values = {});

class Console {
 public:
  void setup(log::Level level);
  void close();
  void logIfExists(const std::string& fileName,
                   int lineNumber,
                   Level level,
                   const std::string& message,
                   const std::map<std::string, LogValue>& values = {});
  bool exists();  // Only to be used for anyLoggerExists(), do not use as an api call

  void logThrottledIfExists(const std::string& fileName,
                            int lineNumber,
                            Level level,
                            double period,
                            const std::string& message,
                            const std::map<std::string, LogValue>& values = {});

  void flush();

 private:
  void log(const std::string& fileName,
           int lineNumber,
           spdlog::level::level_enum level,
           const std::string& message,
           const std::map<std::string, LogValue>& values);
  void logThrottled(double period,
                    const std::string& fileName,
                    int lineNumber,
                    Level level,
                    const std::string& message,
                    const std::map<std::string, LogValue>& values = {});
  std::string hexIdentifier = "hex:";
  std::shared_ptr<spdlog::async_logger> logger;
  std::string createFormattedLine(const std::map<std::string, LogValue>& values);
  std::map<std::pair<std::string, int>, std::chrono::time_point<std::chrono::high_resolution_clock>> throttleStorage;
  std::mutex mutex;
};

class File {
 public:
  void setup(log::Level level,
             const std::string& dir,
             const std::string& textFileName,
             const std::string& binaryFilePrefix_);
  void close();
  void logIfExists(const std::string& fileName,
                   int lineNumber,
                   Level level,
                   const std::string& message,
                   const std::map<std::string, LogValue>& values = {});
  void logThrottledIfExists(const std::string& fileName,
                            int lineNumber,
                            Level level,
                            double period,
                            const std::string& message,
                            const std::map<std::string, LogValue>& values = {});

  bool exists();  // Only to be used for anyLoggerExists(), do not use as an api call

  void flush();

 private:
  void log(const std::string& fileName,
           int lineNumber,
           spdlog::level::level_enum level,
           const std::string& message,
           const std::map<std::string, LogValue>& values);
  void logThrottled(double period,
                    const std::string& fileName,
                    int lineNumber,
                    Level level,
                    const std::string& message,
                    const std::map<std::string, LogValue>& values = {});
  void exchangeSink(const std::string& file, log::Level level);
  std::string createFormattedLine(const std::map<std::string, LogValue>& values, const std::string& file);
  std::shared_ptr<spdlog::async_logger> loggerText;
  std::shared_ptr<spdlog::async_logger> loggerBinary;
  const std::string imageIdentifier = "Image";
  std::string binaryFilePath = "";
  std::string binaryFilePrefix = "";
  std::tuple<std::map<std::string, LogValue>, std::map<std::string, LogValue>> filterBinary(
      const std::map<std::string, LogValue>& values);
  std::map<std::pair<std::string, int>, std::chrono::time_point<std::chrono::high_resolution_clock>> throttleStorage;
  std::mutex mutex;
};

class CSV {
 public:
  void setup(log::Level level,
             const std::string& dir,
             const std::string& csvFileName,
             const std::vector<std::string>& keys);

  void logThrottledIfExists(const std::string& fileName,
                            int lineNumber,
                            Level level,
                            double period,
                            const std::string& message,
                            const std::map<std::string, LogValue>& values = {});
  void close();
  void logIfExists(const std::string& fileName,
                   int lineNumber,
                   spdlog::level::level_enum level,
                   const std::string& message,
                   const std::map<std::string, LogValue>& values = {});

  bool exists();  // Only to be used for anyLoggerExists(), do not use as an api call
  void flush();

 private:
  void log(const std::string& fileName,
           int lineNumber,
           spdlog::level::level_enum level,
           const std::string& message,
           const std::map<std::string, LogValue>& values);
  void logThrottled(double period,
                    const std::string& fileName,
                    int lineNumber,
                    Level level,
                    const std::string& message,
                    const std::map<std::string, LogValue>& values = {});
  void logCSVHeader(const Level& level, std::shared_ptr<spdlog::sinks::basic_file_sink_mt>& csvFileSink) const;
  std::shared_ptr<spdlog::async_logger> logger;
  std::vector<std::string> csvKeys;
  std::map<std::pair<std::string, int>, std::chrono::time_point<std::chrono::high_resolution_clock>> throttleStorage;
  std::mutex mutex;
};

extern Console ConsoleLogger;
extern File FileLogger;
extern CSV CSVLogger;

}  // namespace sv::log
