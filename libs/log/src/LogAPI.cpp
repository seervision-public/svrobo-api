#include "LogAPI.h"
#include "Log.h"

namespace sv::log {
void setupConsole(Level level) { ConsoleLogger.setup(level); }
void setupFile(Level level,
               const std::string& dir,
               const std::string& textFileName,
               const std::string& binaryFilePrefix) {
  FileLogger.setup(level, dir, textFileName, binaryFilePrefix);
}
void setupCsv(Level level,
              const std::string& dir,
              const std::string& csvFileName,
              const std::vector<std::string>& keys) {
  CSVLogger.setup(level, dir, csvFileName, keys);
}

void close() {
  ConsoleLogger.close();
  FileLogger.close();
  CSVLogger.close();
}

void flush() {
  ConsoleLogger.flush();
  FileLogger.flush();
  CSVLogger.flush();
}

}  // namespace sv::log
