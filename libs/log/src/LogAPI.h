#include "Log.h"
#pragma once

/* We have three loggers

  Console: Logs to the console
  File: Logs to a log file, can also log binary data to a separate file
  CSV: Logs to a csv file

  To use, each logger must be setup in the main() of a program. For example:
  #include "Log_API.h"

  sv::log::setupConsole(sv::log::Level::info);
  sv::log::setupFile(sv::log::Level::info, "logs", "file_logger_test","");
  sv::log::setupCsv(sv::log::Level::warn, dir, "csv_logger_test", {"b","a"});

  The csv logger only logs variables that are define in the setup (in the line above this is "a" and "b")

  The file logger has also a binary file sink. It listens to the key "Image". Then, the data is written to a binary
  file.

  Logging calls are done via macros indicating their log level:

  LOG_INFO("My info message", {
    {"a",1},
    {"b",2.0},
    {"c",true},
    {"Image", std::vector<uint8_t>(image, image + imageSize)}
  });

  The first argument is a message string. It is mandatory.
  The second argument is a map of strings to any primitive type. It is optional.

  The log can be throttled by using
  LOG_DEBUG_THROTTLE( time_in_seconds, "message", dataMap)

  You can find some example in test/LogTest.cpp

 */
namespace sv::log {

using Level = spdlog::level::level_enum;

void setupConsole(Level level);
void setupFile(Level level,
               const std::string& dir,
               const std::string& textFileName,
               const std::string& binaryFilePrefix_ = "");
void setupCsv(Level level,
              const std::string& dir,
              const std::string& csvFileName,
              const std::vector<std::string>& keys);

void close();
void flush();
}  // namespace sv::log
#define LOG_TRACE(...) sv::log::log(SPDLOG_FILE_BASENAME(__FILE__), __LINE__, sv::log::Level::trace, __VA_ARGS__);
#define LOG_DEBUG(...) sv::log::log(SPDLOG_FILE_BASENAME(__FILE__), __LINE__, sv::log::Level::debug, __VA_ARGS__);
#define LOG_INFO(...) sv::log::log(SPDLOG_FILE_BASENAME(__FILE__), __LINE__, sv::log::Level::info, __VA_ARGS__);
#define LOG_WARN(...) sv::log::log(SPDLOG_FILE_BASENAME(__FILE__), __LINE__, sv::log::Level::warn, __VA_ARGS__);
#define LOG_ERROR(...) sv::log::log(SPDLOG_FILE_BASENAME(__FILE__), __LINE__, sv::log::Level::err, __VA_ARGS__);
#define LOG_CRITICAL(...) sv::log::log(SPDLOG_FILE_BASENAME(__FILE__), __LINE__, sv::log::Level::critical, __VA_ARGS__);

#define LOG_TRACE_THROTTLE(...) \
  sv::log::logThrottled(SPDLOG_FILE_BASENAME(__FILE__), __LINE__, sv::log::Level::trace, __VA_ARGS__);
#define LOG_DEBUG_THROTTLE(...) \
  sv::log::logThrottled(SPDLOG_FILE_BASENAME(__FILE__), __LINE__, sv::log::Level::debug, __VA_ARGS__);
#define LOG_INFO_THROTTLE(...) \
  sv::log::logThrottled(SPDLOG_FILE_BASENAME(__FILE__), __LINE__, sv::log::Level::info, __VA_ARGS__);
#define LOG_WARN_THROTTLE(...) \
  sv::log::logThrottled(SPDLOG_FILE_BASENAME(__FILE__), __LINE__, sv::log::Level::warn, __VA_ARGS__);
#define LOG_ERROR_THROTTLE(...) \
  sv::log::logThrottled(SPDLOG_FILE_BASENAME(__FILE__), __LINE__, sv::log::Level::err, __VA_ARGS__);
#define LOG_CRITICAL_THROTTLE(...) \
  sv::log::logThrottled(SPDLOG_FILE_BASENAME(__FILE__), __LINE__, sv::log::Level::critical, __VA_ARGS__);

#ifndef SV_RAISE
#define SV_RAISE_DISPATCH(_1, _2, macroName, ...) macroName
#define SV_RAISE(...)                                            \
  SV_RAISE_DISPATCH(__VA_ARGS__, SV_RAISE_2, SV_RAISE_1, UNUSED) \
  (__VA_ARGS__)  // UNUSED argument to avoid compiler warning, see
                 // https://stackoverflow.com/questions/11761703/overloading-macro-on-number-of-arguments
#define SV_RAISE_1(message)            \
  {                                    \
    LOG_ERROR(message, {});            \
    throw std::runtime_error(message); \
  }
#define SV_RAISE_2(message, exception) \
  {                                    \
    LOG_ERROR(message, {});            \
    throw exception;                   \
  }
#endif
