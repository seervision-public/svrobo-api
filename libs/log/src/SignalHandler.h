#pragma once

/**
 * The Signal handler implemented here is taken from backward.hpp. It is adapted to our need (mostly logging via our
 * logging infrastructure instead of stderr).
 *
 * To use it you need to:
 *  - include "SignalHandler.h
 *  #include "SignalHandler.h"
 *  - call sv::signal_handling::initDefaultSignalHandler(true/false); in the main
 *  sv::signal_handling::initDefaultSignalHandler();
 */

#include <cstdlib>
#include <functional>

#include "backward.hpp"

#include "LogAPI.h"

namespace sv::signal_handling {

void initDefaultSignalHandler(bool reraiseSignal = true);

/// Is set to true by any signal handler that received a signal. Do not check this too frequently, as the atomic_bool
/// look up is expensive.
std::atomic_bool anySignalReceived = false;

enum class SignalType {
  ERROR,
  TERMINATION,
  GENERAL,
};

const std::vector<int> defaultSignals{
    // Signals for which the default action is "Core".
    SIGABRT,  // Abort signal from abort(3)
    SIGBUS,   // Bus error (bad memory access)
    SIGFPE,   // Floating point exception
    SIGILL,   // Illegal Instruction
    SIGIOT,   // IOT trap. A synonym for SIGABRT
    SIGQUIT,  // Quit from keyboard
    SIGSEGV,  // Invalid memory reference
    SIGSYS,   // Bad argument to routine (SVr4)
    SIGTRAP,  // Trace/breakpoint trap
    SIGXCPU,  // CPU time limit exceeded (4.2BSD)
    SIGXFSZ,  // File size limit exceeded (4.2BSD)
};

template<SignalType signalType>
class Handler {
 public:
  Handler(const std::vector<int>& posixSignals = defaultSignals, bool reraise = true) : loaded(false) {
    reraiseSignal = reraise;
    bool success = true;

    const size_t stack_size = 1024 * 1024 * 8;
    stackContent.reset(static_cast<char*>(malloc(stack_size)));
    if (stackContent) {
      stack_t ss;
      ss.ss_sp = stackContent.get();
      ss.ss_size = stack_size;
      ss.ss_flags = 0;
      if (sigaltstack(&ss, nullptr) < 0) {
        success = false;
      }
    } else {
      success = false;
    }

    for (auto s : posixSignals) {
      struct sigaction action;
      memset(&action, 0, sizeof action);
      action.sa_flags = static_cast<int>(SA_SIGINFO | SA_ONSTACK | SA_NODEFER | SA_RESETHAND);
      sigfillset(&action.sa_mask);
      sigdelset(&action.sa_mask, s);
      action.sa_sigaction = &sig_handler;

      int r = sigaction(s, &action, nullptr);
      if (r < 0) {
        success = false;
      }
    }

    loaded = success;
  }

  bool isLoaded() const { return loaded; }

  static void handleSignal(int, siginfo_t* info, void* ctx) {
    anySignalReceived = true;
    mySignalReceived = true;
    ucontext_t* uctx = static_cast<ucontext_t*>(ctx);

    backward::StackTrace st;
    void* error_addr = nullptr;
#ifdef REG_RIP  // x86_64
    error_addr = reinterpret_cast<void*>(uctx->uc_mcontext.gregs[REG_RIP]);
#elif defined(REG_EIP)  // x86_32
    error_addr = reinterpret_cast<void*>(uctx->uc_mcontext.gregs[REG_EIP]);
#endif
    if (error_addr) {
      st.load_from(error_addr, 32);
    } else {
      st.load_here(32);
    }

    backward::Printer printer;
    printer.address = true;
    std::stringstream sstream;
    std::string signalName{strsignal(info->si_signo)};
    sstream << "Received signal: " << info->si_signo << " (" << signalName << ")\n";
    printer.print(st, sstream);

    switch (signalType) {
      case SignalType::ERROR:
        LOG_ERROR(sstream.str());
        break;
      case SignalType::TERMINATION:
        LOG_INFO(sstream.str());
        break;
      case SignalType::GENERAL:
        LOG_DEBUG(sstream.str());
        break;
      default:
        LOG_ERROR("Unknown Signal: " + sstream.str());
        break;
    }
    log::flush();

    // Give logger time to write the messages.
    std::this_thread::sleep_for(std::chrono::milliseconds(10));

#if _XOPEN_SOURCE >= 700 || _POSIX_C_SOURCE >= 200809L
    psiginfo(info, nullptr);
#else
    (void)info;
#endif
  }

  static bool mySignalReceived;

 private:
  static bool reraiseSignal;
  backward::details::handle<char*> stackContent;
  bool loaded;
  static void sig_handler(int signo, siginfo_t* info, void* ctx) {
    handleSignal(signo, info, ctx);

    if (reraiseSignal) {
      // try to forward the signal.
      raise(info->si_signo);

      // terminate the process immediately.
      puts("watf? exit");
      _exit(EXIT_FAILURE);
    }
  }
};

template<SignalType signalType>
bool Handler<signalType>::mySignalReceived = false;
template<SignalType signalType>
bool Handler<signalType>::reraiseSignal = true;

static std::unique_ptr<Handler<SignalType::ERROR>> errorHandler;
static std::unique_ptr<Handler<SignalType::TERMINATION>> terminationHandler;
static std::unique_ptr<Handler<SignalType::GENERAL>> generalSignalHandler;

const std::vector<int> errorSignals = {
    SIGSEGV,
    SIGABRT,
    SIGIOT,
    SIGQUIT,
    SIGBUS,
    SIGILL,
    SIGSYS,
    SIGFPE,
    SIGXCPU,
    SIGXFSZ,
};

const std::vector<int> terminationSignals = {
    SIGINT,
    SIGTERM,
};

const std::vector<int> generalSignals = {
    SIGTRAP,
    SIGFPE,
    SIGHUP,
    SIGPIPE,
    SIGALRM,
    SIGUSR1,
    SIGUSR2,
    SIGPOLL,
    SIGPROF,
    SIGVTALRM,
    SIGIO,
    SIGPWR,
};

void initDefaultSignalHandler(bool reraiseSignals) {
  errorHandler = std::make_unique<Handler<SignalType::ERROR>>(errorSignals, reraiseSignals);
  terminationHandler = std::make_unique<Handler<SignalType::TERMINATION>>(terminationSignals, reraiseSignals);
  generalSignalHandler = std::make_unique<Handler<SignalType::GENERAL>>(generalSignals, reraiseSignals);
}
}  // namespace sv::signal_handling
