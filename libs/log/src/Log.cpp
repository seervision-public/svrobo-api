#include "Log.h"  // The log level gets set in this header, that is why we want it before the spdlog headers.
#include <algorithm>
#include <stdexcept>
#include <vector>
#include "LogAPI.h"
#include "Utils.h"
#include "spdlog/async.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"
namespace sv::log {

CSV CSVLogger;
File FileLogger;
Console ConsoleLogger;

void log(const std::string& fileName,
         int lineNumber,
         Level level,
         const std::string& message,
         std::map<std::string, LogValue>&& values) {
  ConsoleLogger.logIfExists(fileName, lineNumber, level, message, values);
  FileLogger.logIfExists(fileName, lineNumber, level, message, values);
  CSVLogger.logIfExists(fileName, lineNumber, level, message, values);
}

void logThrottled(const std::string& fileName,
                  int lineNumber,
                  Level level,
                  double period,
                  const std::string& message,
                  std::map<std::string, LogValue>&& values) {
  ConsoleLogger.logThrottledIfExists(fileName, lineNumber, level, period, message, values);
  FileLogger.logThrottledIfExists(fileName, lineNumber, level, period, message, values);
  CSVLogger.logThrottledIfExists(fileName, lineNumber, level, period, message, values);
}

static bool anyLoggerExists() { return ConsoleLogger.exists() || FileLogger.exists() || CSVLogger.exists(); }

void Console::setup(log::Level level) {
  if (!anyLoggerExists()) {
    spdlog::init_thread_pool(8192, 1);
  }
  if (!exists()) {
    auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();

    std::string pattern = "[%Y-%m-%d %T:%f%@] %^[%l] %v%$";
    console_sink->set_pattern(pattern);

    std::vector<spdlog::sink_ptr> sinks{console_sink};
    logger = std::make_shared<spdlog::async_logger>(
        "console", sinks.begin(), sinks.end(), spdlog::thread_pool(), spdlog::async_overflow_policy::block);
  }

  logger->sinks().at(0)->set_level(level);
  logger->set_level(level);
  logger->flush_on(spdlog::level::err);
}

void Console::logIfExists(const std::string& fileName,
                          int lineNumber,
                          spdlog::level::level_enum level,
                          const std::string& message,
                          const std::map<std::string, LogValue>& values) {
  if (exists()) {
    log(fileName, lineNumber, level, message, values);
  }
}

void Console::log(const std::string& fileName,
                  int lineNumber,
                  spdlog::level::level_enum level,
                  const std::string& message,
                  const std::map<std::string, LogValue>& values) {
  std::string consoleFormattedValues = ConsoleLogger.createFormattedLine(values);
  ConsoleLogger.logger->log(level, "{}:{} {} {}", fileName, lineNumber, message, consoleFormattedValues);
}

std::string Console::createFormattedLine(const std::map<std::string, LogValue>& values) {
  auto fold = [&](std::string accumulated, std::pair<std::string, LogValue> element) {
    return accumulated + "\n" + element.first + ": " +
           std::visit(
               [&](const auto& t) {
                 bool printHex = startsWith(element.first, hexIdentifier);
                 if (printHex) {
                   return to_hex_string(t);
                 } else {
                   return std::to_string(t);
                 }
               },
               element.second);
  };
  return std::accumulate(values.begin(), values.end(), std::string(), fold);
}

void Console::close() { logger.reset(); }

bool Console::exists() { return bool(logger); }

void Console::logThrottledIfExists(const std::string& fileName,
                                   int lineNumber,
                                   Level level,
                                   double period,
                                   const std::string& message,
                                   const std::map<std::string, LogValue>& values) {
  if (exists()) {
    logThrottled(period, fileName, lineNumber, level, message, values);
  }
}

void Console::logThrottled(double period,
                           const std::string& fileName,
                           int lineNumber,
                           Level level,
                           const std::string& message,
                           const std::map<std::string, LogValue>& values) {
  std::lock_guard guard(mutex);
  std::pair<std::string, int> key = std::pair<std::string, int>(fileName, lineNumber);
  auto it = throttleStorage.find(key);
  bool exists = (it != throttleStorage.end());
  if (exists) {
    auto timeDiff = std::chrono::high_resolution_clock::now() - throttleStorage[key];
    auto duration = std::chrono::milliseconds(static_cast<uint64_t>(period * 1000));
    if (timeDiff > duration) {
      log(fileName, lineNumber, level, message, values);
      throttleStorage[key] = std::chrono::high_resolution_clock::now();
    }
  } else {
    throttleStorage[key] = std::chrono::high_resolution_clock::now();
    log(fileName, lineNumber, level, message, values);
  }
}
void Console::flush() {
  if (exists()) {
    logger->flush();
  }
}

void File::setup(log::Level level,
                 const std::string& dir,
                 const std::string& textFileName,
                 const std::string& binaryFilePrefix_) {
  if (!anyLoggerExists()) {
    spdlog::init_thread_pool(8192, 1);
  }

  binaryFilePath = dir;
  binaryFilePrefix = binaryFilePrefix_;
  if (!exists()) {
    if (!createDir(dir)) {
      std::cerr << "LOGGER ERROR: Failed to setup file logger: Could not create directory " << dir << std::endl;
      return;
    }

    std::string filePath = addPaths(dir, textFileName + ".log");
    auto textFileSink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(filePath, 1024 * 1024, 4, false);
    std::string textFileSinkPattern = "[%Y-%m-%d %T:%f%@] %^[%l] %v%$";
    textFileSink->set_pattern(textFileSinkPattern);
    std::vector<spdlog::sink_ptr> sinks{textFileSink};
    loggerText = std::make_shared<spdlog::async_logger>(
        "text", sinks.begin(), sinks.end(), spdlog::thread_pool(), spdlog::async_overflow_policy::block);

    // Use the textFileSink, the Sink will be deleted before the log call to give the bin file a new name.
    loggerBinary = std::make_shared<spdlog::async_logger>(
        "binary", textFileSink, spdlog::thread_pool(), spdlog::async_overflow_policy::block);
  }

  loggerBinary->set_level(level);
  loggerBinary->flush_on(spdlog::level::err);
  loggerText->sinks().at(0)->set_level(level);
  loggerText->set_level(level);
  loggerText->flush_on(spdlog::level::err);
}

void File::logIfExists(const std::string& fileName,
                       int lineNumber,
                       spdlog::level::level_enum level,
                       const std::string& message,
                       const std::map<std::string, LogValue>& values) {
  if (exists()) {
    log(fileName, lineNumber, level, message, values);
  }
}

void File::log(const std::string& fileName,
               int lineNumber,
               spdlog::level::level_enum level,
               const std::string& message,
               const std::map<std::string, LogValue>& values) {
  auto [valuesFiltered, binaryValues] = FileLogger.filterBinary(values);

  // Only logging of one image per LOG call is supported.
  // In a previous solution to log multiple images we got random segmentation faults for unknown reason.
  std::string binaryFileName = "";
  if (!binaryValues.empty()) {
    std::string currentTime = getCurrentTime();
    auto key = binaryValues.begin()->first;
    auto value = binaryValues.begin()->second;
    binaryFileName = FileLogger.binaryFilePrefix + key + "_" + currentTime + ".bin";
    std::string binaryFile = addPaths(FileLogger.binaryFilePath, binaryFileName);
    FileLogger.exchangeSink(binaryFile, level);  // need to give the bin file a new name

    std::vector<uint8_t> data = std::get<std::vector<uint8_t>>(value);
    FileLogger.loggerBinary->log(level, "{}", data_to_string(data));
    FileLogger.loggerBinary->flush();
  }
  // TODO [stefan.frei] what if there is no image, but we still want to log !
  std::string fileFormattedValues = FileLogger.createFormattedLine(valuesFiltered, binaryFileName);
  FileLogger.loggerText->log(level, "{}:{} {} {}", fileName, lineNumber, message, fileFormattedValues);

  if (binaryValues.size() > 1) {
    LOG_ERROR("Only logging of one image per LOG call is supported.")
  }
}

std::string File::createFormattedLine(const std::map<std::string, LogValue>& values, const std::string& file) {
  auto fold = [&](std::string accumulated, std::pair<std::string, LogValue> element) {
    return accumulated + "\n" + element.first + ": " +
           std::visit(
               [&](const auto& t) {
                 if (startsWith(element.first, File::imageIdentifier)) {
                   return file;
                 } else {
                   return std::to_string(t);
                 }
               },
               element.second);
  };
  return std::accumulate(values.begin(), values.end(), std::string(), fold);
}

std::tuple<std::map<std::string, LogValue>, std::map<std::string, LogValue>> File::filterBinary(
    const std::map<std::string, LogValue>& values) {
  std::map<std::string, LogValue> binaryData;
  std::copy_if(values.begin(), values.end(), std::inserter(binaryData, binaryData.begin()), [&](auto const& element) {
    return startsWith(element.first, imageIdentifier);
  });
  return {values, binaryData};
}

void File::exchangeSink(const std::string& file, log::Level level) {
  auto& sinks = loggerBinary->sinks();
  auto binaryFileSink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(file);
  std::string binaryFileSinkPattern = "%v";
  binaryFileSink->set_pattern(binaryFileSinkPattern);
  sinks.erase(sinks.begin());  // remove the only sink
  sinks.push_back(binaryFileSink);

  loggerBinary->sinks().back()->set_level(level);
}

void File::close() {
  loggerText.reset();
  loggerBinary.reset();
}

bool File::exists() { return bool(loggerText) && bool(loggerBinary); }

void File::logThrottledIfExists(const std::string& fileName,
                                int lineNumber,
                                Level level,
                                double period,
                                const std::string& message,
                                const std::map<std::string, LogValue>& values) {
  if (exists()) {
    logThrottled(period, fileName, lineNumber, level, message, values);
  }
}

void File::logThrottled(double period,
                        const std::string& fileName,
                        int lineNumber,
                        Level level,
                        const std::string& message,
                        const std::map<std::string, LogValue>& values) {
  std::lock_guard guard(mutex);
  auto key = std::pair<std::string, int>(fileName, lineNumber);
  auto it = throttleStorage.find(key);
  bool exists = (it != throttleStorage.end());
  if (exists) {
    auto timeDiff = std::chrono::high_resolution_clock::now() - throttleStorage[key];
    auto duration = std::chrono::milliseconds(static_cast<uint64_t>(period * 1000));
    if (timeDiff > duration) {
      log(fileName, lineNumber, level, message, values);
      throttleStorage[key] = std::chrono::high_resolution_clock::now();
    }
  } else {
    throttleStorage[key] = std::chrono::high_resolution_clock::now();
    log(fileName, lineNumber, level, message, values);
  }
}

void File::flush() {
  if (exists()) {
    loggerText->flush();
    loggerBinary->flush();
  }
}

void CSV::setup(log::Level level,
                const std::string& dir,
                const std::string& csvFileName,
                const std::vector<std::string>& keys) {
  if (!anyLoggerExists()) {
    spdlog::init_thread_pool(8192, 1);
  }

  if (!exists()) {
    if (!createDir(dir)) {
      std::cerr << "LOGGER ERROR: CSV logger failed to setup: Could not create directory " << dir << std::endl;
      return;
    }
    csvKeys = keys;
    std::string csvPathAndName = addPaths(dir, csvFileName + ".csv");
    auto csvFileSink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(csvPathAndName);
    std::vector<spdlog::sink_ptr> sinks{csvFileSink};

    logCSVHeader(level, csvFileSink);

    logger = std::make_shared<spdlog::async_logger>(
        "csv", sinks.begin(), sinks.end(), spdlog::thread_pool(), spdlog::async_overflow_policy::block);
    // Set general Pattern to include the system time
    std::string generalPattern = "\"%Y-%m-%d %T:%f\"%v";
    csvFileSink->set_pattern(generalPattern);
    logger->sinks().at(0)->set_level(level);
    logger->set_level(level);
  }

  logger->sinks().at(0)->set_level(level);
  logger->set_level(level);
  logger->flush_on(spdlog::level::err);
}

void CSV::logCSVHeader(const Level& level, std::shared_ptr<spdlog::sinks::basic_file_sink_mt>& csvFileSink) const {
  std::string headerPattern = "%v";
  csvFileSink->set_pattern(headerPattern);

  auto headerLogger = spdlog::logger("csvHeader", csvFileSink);
  // log header line
  auto fold = [&](std::string accumulated, std::string element) { return accumulated + "," + element; };
  std::string header =
      std::accumulate(csvKeys.begin(), csvKeys.end(), std::string("SystemTime,FileName,Line,Message"), fold);

  headerLogger.sinks().at(0)->set_level(level);
  headerLogger.set_level(level);
  headerLogger.log(level, header);
}

void CSV::logIfExists(const std::string& fileName,
                      int lineNumber,
                      spdlog::level::level_enum level,
                      const std::string& message,
                      const std::map<std::string, LogValue>& values) {
  if (exists()) {
    log(fileName, lineNumber, level, message, values);
  }
}

void CSV::log(const std::string& fileName,
              int lineNumber,
              spdlog::level::level_enum level,
              const std::string& message,
              const std::map<std::string, LogValue>& values) {
  if (!values.empty()) {
    std::vector<std::string> items;
    items.push_back(fileName);
    items.push_back(std::to_string(lineNumber));
    items.push_back(message);

    for (const auto& csvKey : csvKeys) {
      if (values.find(csvKey) != values.end()) {
        items.push_back(std::visit([&](const auto& t) { return std::to_string(t); }, values.at(csvKey)));
      } else {
        items.push_back("");
      }
    }

    auto csvLine = std::accumulate(items.begin(), items.end(), std::string(), [&](auto accumulated, auto value) {
      return accumulated + "," + escapeCsv(value);
    });

    CSVLogger.logger->log(level, csvLine);
  }
}

void CSV::close() { logger.reset(); }

bool CSV::exists() { return bool(logger); }

void CSV::logThrottledIfExists(const std::string& fileName,
                               int lineNumber,
                               Level level,
                               double period,
                               const std::string& message,
                               const std::map<std::string, LogValue>& values) {
  if (exists()) {
    logThrottled(period, fileName, lineNumber, level, message, values);
  }
}

void CSV::logThrottled(double period,
                       const std::string& fileName,
                       int lineNumber,
                       Level level,
                       const std::string& message,
                       const std::map<std::string, LogValue>& values) {
  std::lock_guard guard(mutex);
  auto key = std::pair<std::string, int>(fileName, lineNumber);
  auto it = throttleStorage.find(key);
  bool exists = (it != throttleStorage.end());
  if (exists) {
    auto timeDiff = std::chrono::high_resolution_clock::now() - throttleStorage[key];
    auto duration = std::chrono::milliseconds(static_cast<uint64_t>(period * 1000));
    if (timeDiff > duration) {
      log(fileName, lineNumber, level, message, values);
      throttleStorage[key] = std::chrono::high_resolution_clock::now();
    }
  } else {
    throttleStorage[key] = std::chrono::high_resolution_clock::now();
    log(fileName, lineNumber, level, message, values);
  }
}

void CSV::flush() {
  if (exists()) {
    logger->flush();
  }
}

}  // namespace sv::log
