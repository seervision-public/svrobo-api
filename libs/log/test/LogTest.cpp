#include <gtest/gtest.h>

#include <fstream>
#include <map>
#include <variant>
#include "LogAPI.h"
#include "Utils.h"

namespace sv {

// TODO [stefan.frei] RD-1380 These tests are just to generate the outputs. They don't check anything yet.
// The output is in catkin_ws/build/libs/lib
// what need be done:
// -- add checks

class LogTest : public ::testing::Test {
 public:
  static void SetUpTestCase() {
    if (!deleteDir(logDir)) {
      std::cerr << "Failed to delete directory: " << logDir << std::endl;
    }
  }
  void TearDown() { sv::log::close(); }
  static std::string logDir;
};
std::string LogTest::logDir = "logs";

TEST_F(LogTest, ConsoleLog) {
  sv::log::setupConsole(sv::log::Level::debug);
  std::string message = "mmmm";

  uint8_t image[] = {10, 20, 30};
  auto imageSize = sizeof(image) / sizeof(image[0]);
  short x = 5;
  LOG_INFO(
      message,
      {{"hex:a", 12}, {"b", 2.0}, {"c", true}, {"x", x}, {"Image", std::vector<uint8_t>(image, image + imageSize)}});
}

TEST_F(LogTest, ConsoleLogThrottled) {
  sv::log::setupConsole(sv::log::Level::debug);
  std::string message = "mmmm";

  // Print only every 200 ms
  double period = 0.2;
  for (int i = 0; i < 100; ++i) {
    LOG_DEBUG_THROTTLE(period, message, {{"counter", i}});
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  // It should be printed 5 times
}

TEST_F(LogTest, FileLog) {
  sv::log::setupFile(sv::log::Level::info, logDir, "file_logger_test", "");
  std::string message = "mmmm";

  uint8_t image[] = {10, 20, 30};
  auto imageSize = sizeof(image) / sizeof(image[0]);

  LOG_INFO(message, {{"a", 1}, {"b", 2.0}, {"c", true}, {"Image", std::vector<uint8_t>(image, image + imageSize)}});
  LOG_INFO("helloe",
           {
               {"a", 1},
           });
  LOG_INFO("only message")
}

TEST_F(LogTest, FileLogThrottled) {
  sv::log::setupFile(sv::log::Level::debug, logDir, "file_logger_test_throttled", "");
  std::string message = "mmmm";

  // Print only every 200 ms
  double period = 0.2;
  for (int i = 0; i < 100; ++i) {
    LOG_DEBUG_THROTTLE(period, message, {{"counter", i}});
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  // Wait until the file is written
  sv::log::flush();
  std::this_thread::sleep_for(std::chrono::milliseconds(100));
  // It should be printed 5 times (100 *10 ms / 200 ms = 5), hence 10 lines
  // Sometimes the sleep is incorrect and thus it may print 12 lines
  std::ifstream logFile(logDir + "/file_logger_test_throttled.log");
  ASSERT_TRUE(logFile.is_open());

  std::string line;
  int numberOfLines = 0;
  while (std::getline(logFile, line)) {
    ++numberOfLines;
  }
  EXPECT_LE(numberOfLines, 12);
  EXPECT_GE(numberOfLines, 10);
}

TEST_F(LogTest, csvLog) {
  sv::log::setupCsv(sv::log::Level::warn, logDir, "csv_logger_test", {"b", "a"});
  std::string message = "mmmm";

  uint8_t image[] = {10, 20, 30};
  auto imageSize = sizeof(image) / sizeof(image[0]);
  LOG_WARN(message, {{"a", 1}, {"b", 2.0}, {"c", true}, {"Image", std::vector<uint8_t>(image, image + imageSize)}});
  LOG_WARN(message,
           {
               {"b", 11},
               {"a", 10},
           });

  LOG_WARN(message,
           {
               {"b", 21},
           });
  LOG_WARN(message,
           {
               {"a", 100},
           });
  LOG_WARN("only a message");                      // should not create a line in csv file
  LOG_WARN("this, that", {{"a", 10}, {"b", 20}});  // The comma should not be interpreted as new column
  LOG_WARN("strings",
           {{"a", std::string("hi\", there")},
            {"b", std::string("answer,me, please")}});  // The comma should not be interpreted as new column
}

TEST_F(LogTest, csvLogThrottled) {
  sv::log::setupCsv(sv::log::Level::trace, logDir, "csv_logger_test_throttled", {"counter"});
  std::string message = "mmmm";

  // Print only every 200 ms
  double period = 0.2;
  for (int i = 0; i < 100; ++i) {
    LOG_DEBUG_THROTTLE(period, message, {{"counter", i}});
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }

  // Wait until the file is written
  sv::log::flush();
  std::this_thread::sleep_for(std::chrono::milliseconds(100));
  // It should be printed 5 times (100 *10 ms / 200 ms = 5), hence 5 + 1 header lines
  // Sometimes the sleep is incorrect and thus it may print 7 lines
  std::ifstream logFile(logDir + "/csv_logger_test_throttled.csv");
  ASSERT_TRUE(logFile.is_open());

  std::string line;
  int numberOfLines = 0;
  while (std::getline(logFile, line)) {
    ++numberOfLines;
  }
  EXPECT_LE(numberOfLines, 7);
  EXPECT_GE(numberOfLines, 6);
}

TEST_F(LogTest, all) {
  sv::log::setupConsole(sv::log::Level::err);
  sv::log::setupCsv(sv::log::Level::err, logDir, "all_csv_logger_test", {"a", "b"});
  sv::log::setupFile(sv::log::Level::err, logDir, "all_file_logger_test", "all_");

  std::string message = "mmmm";

  uint8_t image_1[] = {10, 20, 30};
  uint8_t image_2[] = {10, 20, 30, 40};
  auto imageSize_1 = sizeof(image_1) / sizeof(image_1[0]);
  auto imageSize_2 = sizeof(image_2) / sizeof(image_2[0]);
  LOG_ERROR(message,
            {
                {"a", 1},
                {"b", 2.0},
                {"c", true},
                {"Image_1", std::vector<uint8_t>(image_1, image_1 + imageSize_1)},
            });
  LOG_ERROR(message,
            {
                {"a", 1},
                {"b", 2.0},
                {"c", true},
                {"Image_2", std::vector<uint8_t>(image_1, image_1 + imageSize_1)},
                {"Image_3", std::vector<uint8_t>(image_2, image_2 + imageSize_2)},
            });
  LOG_ERROR("only message")
}

// Temporary to generate csv data

TEST_F(LogTest, csvComplete) {
  sv::log::setupCsv(sv::log::Level::warn, logDir, "complete", {"Time", "b", "a"});
  std::string message = "one message";
  uint8_t image[] = {10, 20, 30};
  auto imageSize = sizeof(image) / sizeof(image[0]);
  LOG_WARN(message,
           {
               {"Time", 0},
               {"a", 1},
               {"b", 2.0},
           });
  LOG_WARN(message,
           {
               {"Time", 1},
               {"a", 1.1},
               {"b", 2.1},
           });
  LOG_WARN(message,
           {
               {"Time", 2},
               {"a", 1.2},
               {"b", 1.9},
           });
  LOG_WARN(message,
           {
               {"Time", 3},
               {"a", 1},
               {"b", 2.0},
           });
  LOG_WARN(message,
           {
               {"Time", 4},
               {"a", 0.5},
               {"b", 2.0},
           });
}

TEST_F(LogTest, csvIncomplete) {
  sv::log::setupCsv(sv::log::Level::warn, logDir, "incomplete", {"b", "a"});
  std::string message = "one message";

  uint8_t image[] = {10, 20, 30};
  auto imageSize = sizeof(image) / sizeof(image[0]);
  LOG_WARN(message,
           {
               {"a", 1},
               {"b", 2.0},
           });
  LOG_WARN(message,
           {
               {"a", 1.1},
           });
  LOG_WARN(message,
           {
               {"a", 1.2},
           });
  LOG_WARN(message,
           {
               {"b", 2.0},
           });
  LOG_WARN(message,
           {
               {"b", 1.9},
           });
  LOG_WARN(message,
           {
               {"a", 0.8},
               {"b", 1.7},
           });
  LOG_WARN(message,
           {
               {"a", 0.1},
               {"b", 2.0},
           });
}

}  // namespace sv
