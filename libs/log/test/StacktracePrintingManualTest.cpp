#include <signal.h>

#include <CLI11.hpp>

#include "LogAPI.h"
#include "SignalHandler.h"

static void printFlushAndWait(const std::string& msg) {
  LOG_INFO(msg, {});
  spdlog::default_logger()->flush();
  std::this_thread::sleep_for(std::chrono::milliseconds(10));
}

static void third(int fault) {
  int* p;
  int a;
  switch (fault) {
    case 0:
      printFlushAndWait("Throwing an exception. Expecting an abort signal (6).\n");
      throw std::runtime_error("Hello! I am an exception :)");
    case 1:
      printFlushAndWait("Raising a SIGSEGV (11).");
      raise(SIGSEGV);
      break;  // prevent compiler warning
    case 2:
      printFlushAndWait("Dereferencing some random memory. Expecting an segmentation fault (11).");
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
      a = *(p + 1'000'000'000);
#pragma GCC diagnostic pop
      break;  // prevent compiler warning
    case 3:
      printFlushAndWait("Calling std::abort(). Expecting an abort signal (6).");
      std::abort();
      break;  // prevent compiler warning
    case 4:
      printFlushAndWait("Calling std::terminate(). Expecting an abort signal (6).");
      std::terminate();
    case 5:
      printFlushAndWait("Calling std::quick_exit(1). Expecting no signals.");
      std::quick_exit(1);
    case 6:
      printFlushAndWait("Calling std::exit(1). Expecting no signals.");
      std::exit(1);
    default:
      printFlushAndWait("Run out of options. I'll give you an exception nevertheless :)");
      throw std::runtime_error("Can you catch me?");
  }
}

static void second(int fault) { third(fault); }

static void first(int fault) { second(fault); }

struct Args {
  int fault = 0;
};

static Args parseArgs(int argc, char** argv) {
  Args args{};
  CLI::App app;
  app.add_option("--fault", args.fault, "Type of error to create")->check(CLI::IsMember({0, 1, 2, 3, 4, 5, 6}));

  try {
    app.parse(argc, argv);
  } catch (const CLI::ParseError& e) {
    std::exit(app.exit(e));
  }
  return args;
}

int main(int argc, char** argv) {
  Args args = parseArgs(argc, argv);
  sv::log::ConsoleLogger.setup(sv::log::Level::trace);
  sv::signal_handling::initDefaultSignalHandler(false);

  LOG_INFO("First we expect the signals to be caught and logged, but the program should continue to run:");
  std::this_thread::sleep_for(std::chrono::seconds(1));
  printFlushAndWait("Raising SIGSEGV");
  raise(SIGSEGV);
  std::this_thread::sleep_for(std::chrono::seconds(1));

  printFlushAndWait("Raising SIGINT");
  raise(SIGINT);
  std::this_thread::sleep_for(std::chrono::seconds(1));

  printFlushAndWait("Raising SIGPOLL");
  raise(SIGPOLL);
  std::this_thread::sleep_for(std::chrono::seconds(1));

  sv::signal_handling::initDefaultSignalHandler(true);
  LOG_INFO("Now the program will be killed. Use the --fault argument to create different errors.");

  first(args.fault);

  return 0;
}
