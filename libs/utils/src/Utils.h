#pragma once

#include <sys/stat.h>
#include <unistd.h>

#include <chrono>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <optional>
#include <regex>
#include <sstream>
#include <stdexcept>
#include <utility>
#include <vector>

namespace std {

inline std::string to_string(bool b) {
  if (b) {
    return "<true>";
  } else {
    return "<false>";
  };
}
inline std::string to_string(const std::string& s) { return s; }
inline std::string to_string(const std::vector<uint8_t>& binArray) {
  return "<binary array of size " + std::to_string(binArray.size()) + ">";
}
inline std::string to_string(const std::vector<char>& vec) { return std::string(vec.begin(), vec.end()); }

}  // namespace std

namespace sv {

/**
 * @brief Limits value to be inside [lowerBound, upperBound]
 * @tparam T
 * @param value
 * @param lowerBound
 * @param upperBound
 * @return Returns the limited value and a boolean that is true if limiting took place, i.e., if value was not in
 * [lowerBound, upperBound]
 */
template<typename T>
std::pair<T, bool> limit(const T& value, const T& lowerBound, const T& upperBound) {
  if (lowerBound > upperBound) {
    throw(std::invalid_argument(
        std::string("Lower bound " + std::to_string(lowerBound) + " > upper bound " + std::to_string(upperBound))));
  }

  if (value < lowerBound) {
    return std::make_pair(lowerBound, true);
  } else if (value > upperBound) {
    return std::make_pair(upperBound, true);
  }
  return std::make_pair(value, false);
}

template<typename T>
bool inRange(const T& value, const T& lowerBound, const T& upperBound) {
  return (lowerBound <= value) && (value <= upperBound);
}

template<class ContainerOfFutures>
void waitForAll(ContainerOfFutures& handles) {
  for (auto& h : handles) {
    h.get();
  }
}

/**
 * @brief Return standard output of shell command as string
 * @param cmd
 * @return Return standard output of shell command as string
 */
std::string exec(const std::string& cmd);
bool dirExists(const std::string& dir);
bool createDir(const std::string& dir);
bool deleteDir(const std::string& dir);

bool fileExists(const std::string& file);
bool deleteFile(const std::string& file);

template<class... Args>
std::string addPaths(const Args&... args) {
  std::stringstream newPath;
  for (const auto& path : {args...}) {
    newPath << path;
    if (path.back() != '/') {
      newPath << "/";
    }
  }
  auto pathStr = newPath.str();
  pathStr.pop_back();
  return pathStr;
}

std::string getCurrentTime();

// to_hex_string needs an operator<< for vector<unsigned char>
// use this function to print small vectors to console (std::cout)
template<typename Stream>
Stream& operator<<(Stream& stream, const std::vector<unsigned char>& values) {
  for (auto const& element : values) {
    stream << element << " ";
  }
  return stream;
}

template<typename T>
inline std::string to_hex_string(const T& value) {
  std::stringstream stream;
  stream << "0x" << std::setfill('0') << std::setw(sizeof(T) * 2) << std::hex << value;
  return stream.str();
}
template<>
inline std::string to_hex_string<std::vector<char>>(const std::vector<char>& value) {
  // hex for vectors is not supported
  return std::to_string(value);
}

template<class T>
std::string data_to_string(const std::vector<T>& data) {
  return std::string(data.begin(), data.end());
}

bool startsWith(const std::string& mainStr, const std::string& toMatch);
bool startsWithCaseInsensitive(std::string mainStr, std::string toMatch);

void toUpper(std::string& str);
void toLower(std::string& str);
std::string toUpperCopy(const std::string& str);
std::string toLowerCopy(const std::string& str);

std::string replaceDotAndDashWithUnderscore(const std::string& str);

template<class Condition>
bool waitUntil(Condition condition, unsigned int timeoutInMs, unsigned int sleepIntervalInMs = 2) {
  auto start = std::chrono::high_resolution_clock::now();
  std::chrono::milliseconds timeout(timeoutInMs);
  while (!condition() && std::chrono::high_resolution_clock::now() - start < timeout) {
    usleep(sleepIntervalInMs * 1000);
  }
  return condition();
}

inline std::string escapeCsv(const std::string& text) {
  std::regex csvSpecialCharacters("[, \"]+");
  std::regex doubleQuotes("\"");
  std::smatch match;
  if (std::regex_search(text, match, csvSpecialCharacters)) {
    return "\"" + std::regex_replace(text, doubleQuotes, "\"\"") + "\"";
  }
  return text;
}

// https://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c
template<typename T>
int sgn(T val) {
  return (T(0) < val) - (val < T(0));
}

std::string getHostName();

template<typename T>
int getIndexOfClosestValue(const T value, const std::vector<T>& sorted_vec) {
  auto const closestUpperElement = std::upper_bound(sorted_vec.begin(), sorted_vec.end(), value);
  auto const closestLowerElement = closestUpperElement - 1;

  if (std::fabs(*closestLowerElement - value) <= std::fabs(*closestUpperElement - value)) {
    return (closestLowerElement - sorted_vec.begin());
  } else {
    return (closestUpperElement - sorted_vec.begin());
  }
}

// Assumes that v is sorted in ascending order
inline int convertToDiscreteValues(double data,
                                   const std::vector<double>& discreteIncrements,
                                   const std::vector<double>& dataMapping) {
  return discreteIncrements.at(static_cast<unsigned int>(getIndexOfClosestValue(data, dataMapping)));
}

template<typename T>
inline std::string to_string(T value, int precision) {
  std::stringstream stream;
  stream << std::fixed << std::setprecision(precision) << value;
  return stream.str();
}

std::optional<std::string> getEnvironmentVariable(const std::string& variableName);

}  // namespace sv
