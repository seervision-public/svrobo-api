#pragma once

#include "stdint.h"

/**
 * @brief      Convert a value (of type T) to a byte array and writes it into "bytes"; also does endianness conversion
 *
 * @param      bytes             A pointer to byte array to be written to, needs be be allocated and of size at least
 * sizeof(T)
 * @param[in]  value             The value
 * @param[in]  changeEndianness  True if endianness should be changed, e.g. from big ot little endian and vice versa
 *
 * @tparam     T
 */
template<class T>
void convertToByteArray(uint8_t* bytes, T value, bool changeEndianness = false) {
  uint8_t* pValue = reinterpret_cast<uint8_t*>(&value);
  if (changeEndianness) {
    for (unsigned int i = 0; i < sizeof(T); ++i) {
      bytes[i] = pValue[i];
    }
  } else {
    for (unsigned int i = 0; i < sizeof(T); ++i) {
      bytes[i] = pValue[sizeof(T) - 1 - i];
    }
  }
}

/**
 * @brief      Convert a byte array to a value of type T; also does endianness conversion
 *
 * @param[in]  bytes             A pointer to byte array, needs to have size at least sizeof(T)
 * @param[in]  changeEndianness  True if endianness should be changed, e.g. from big ot little endian and vice versa
 *
 * @tparam     T
 *
 * @return     Returns the value
 */
template<class T>
static T convertFromByteArray(const uint8_t* bytes, bool changeEndianness = false) {
  T value;
  uint8_t* pValue = reinterpret_cast<uint8_t*>(&value);
  if (changeEndianness) {
    for (unsigned int i = 0; i < sizeof(T); ++i) {
      pValue[i] = bytes[i];
    }
  } else {
    for (unsigned int i = 0; i < sizeof(T); ++i) {
      pValue[i] = bytes[sizeof(T) - 1 - i];
    }
  }
  return value;
}

inline uint16_t combineTwoBytes(uint8_t first_bytes, uint8_t second_bytes) {
  uint8_t bytes[2] = {second_bytes, first_bytes};
  return convertFromByteArray<uint16_t>(bytes, true);
}

inline uint32_t combineThreeBytes(uint8_t first_bytes, uint8_t second_bytes, uint8_t third_bytes) {
  uint8_t bytes[3] = {first_bytes, second_bytes, third_bytes};
  return convertFromByteArray<uint32_t>(bytes, true);
}

inline uint32_t combineFourBytes(uint8_t first_byte, uint8_t second_byte, uint8_t third_byte, uint8_t fourth_byte) {
  uint8_t bytes[4] = {fourth_byte, third_byte, second_byte, first_byte};
  return convertFromByteArray<uint32_t>(bytes, true);
}

inline int extractSigned24Bit(uint8_t first_byte, uint8_t second_byte, uint8_t third_byte) {
  uint8_t bytes[3] = {first_byte, second_byte, third_byte};
  bool signedBit = bytes[0] & (1 << 7);
  if (signedBit) {
    return -static_cast<int>(combineThreeBytes(~bytes[2], ~bytes[1], ~bytes[0]));
  } else {
    return static_cast<int>(combineThreeBytes(bytes[2], bytes[1], bytes[0]));
  }
}
