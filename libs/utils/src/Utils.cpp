#include "Utils.h"

#include <dirent.h>
#include <sys/stat.h>

#include <algorithm>

#include "date.h"

namespace sv {

std::string getCurrentTime() {
  std::string currentTime = date::format("%F %T", std::chrono::system_clock::now());
  // Replace all weird characters by '-'
  std::replace_if(
      currentTime.begin(), currentTime.end(), [](char c) { return c == ' ' || c == '_' || c == ':' || c == '.'; }, '-');

  return currentTime;
}

/* https://thispointer.com/c-check-if-a-string-starts-with-an-another-given-string/
 * Case Sensitive Implementation of startsWith()
 * It checks if the string 'mainStr' starts with given string 'toMatch'
 */
bool startsWith(const std::string& mainStr, const std::string& toMatch) {
  // std::string::find returns 0 if toMatch is found at starting
  return (mainStr.find(toMatch) == 0);
}

/* https://thispointer.com/c-check-if-a-string-starts-with-an-another-given-string/
 * Case Insensitive Implementation of startsWith()
 * It checks if the string 'mainStr' starts with given string 'toMatch'
 */
bool startsWithCaseInsensitive(std::string mainStr, std::string toMatch) {
  // Convert mainStr to lower case
  std::transform(mainStr.begin(), mainStr.end(), mainStr.begin(), ::tolower);
  // Convert toMatch to lower case
  std::transform(toMatch.begin(), toMatch.end(), toMatch.begin(), ::tolower);
  return startsWith(mainStr, toMatch);
}

// https://stackoverflow.com/questions/18100097/portable-way-to-check-if-directory-exists-windows-linux-c
bool dirExists(const std::string& dir) {
  struct stat info;

  if (stat(dir.c_str(), &info) != 0) {
    return false;
  } else if (info.st_mode & S_IFDIR) {
    return true;
  } else {
    return false;
  }
}

bool createDir(const std::string& dir) {
  if (dirExists(dir)) {
    return true;
  }

  // https://codeyarns.com/2014/08/07/how-to-create-directory-using-c-on-linux/
  return mkdir(dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH) == 0;
}

int removeDir(const char* path);
// https://stackoverflow.com/questions/2256945/removing-a-non-empty-directory-programmatically-in-c-or-c
int removeDir(const char* path) {
  DIR* d = opendir(path);
  size_t path_len = strlen(path);
  int r = -1;

  if (d) {
    struct dirent* p;
    r = 0;
    while (!r && (p = readdir(d))) {
      int r2 = -1;
      char* buf;
      size_t len;

      /* Skip the names "." and ".." as we don't want to recurse on them. */
      if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, "..")) {
        continue;
      }

      len = path_len + strlen(p->d_name) + 2;
      buf = new char[len];

      if (buf) {
        struct stat statbuf;

        snprintf(buf, len, "%s/%s", path, p->d_name);

        if (!stat(buf, &statbuf)) {
          if (S_ISDIR(statbuf.st_mode)) {
            r2 = removeDir(buf);
          } else {
            r2 = unlink(buf);
          }
        }
        delete buf;
      }
      r = r2;
    }
    closedir(d);
  }
  if (!r) {
    r = rmdir(path);
  }
  return r;
}

bool deleteDir(const std::string& dir) { return removeDir(dir.c_str()) == 0; }

std::string exec(const std::string& cmd) {
  std::array<char, 128> buffer;
  std::string result;
  std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd.c_str(), "r"), pclose);
  if (!pipe) {
    throw std::runtime_error("popen() failed!");
  }
  while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
    result += buffer.data();
  }
  return result;
}

bool fileExists(const std::string& file) {
  struct stat buffer;
  return (stat(file.c_str(), &buffer) == 0);
}

bool deleteFile(const std::string& file) {
  int ec = std::remove(file.c_str());
  return ec == 0;
}

void toUpper(std::string& str) {
  std::for_each(str.begin(), str.end(), [](char& c) { c = std::toupper(c); });
}

void toLower(std::string& str) {
  std::for_each(str.begin(), str.end(), [](char& c) { c = std::tolower(c); });
}

std::string toUpperCopy(const std::string& str) {
  std::string copy;
  copy.reserve(str.length());
  for (char c : str) {
    copy.push_back(std::toupper(c));
  }
  return copy;
}

std::string toLowerCopy(const std::string& str) {
  std::string copy;
  copy.reserve(str.length());
  for (char c : str) {
    copy.push_back(std::toupper(c));
  }
  return copy;
}

std::string replaceDotAndDashWithUnderscore(const std::string& str) {
  std::string result = str;
  std::replace(result.begin(), result.end(), '-', '_');
  std::replace(result.begin(), result.end(), '.', '_');
  return result;
}
std::string getHostName() {
  const std::size_t length = 64;
  char hostName[length];
  int ec = gethostname(hostName, length);
  if (ec == 0) {  // success
    std::string hostNameStr(hostName);
    std::replace(hostNameStr.begin(), hostNameStr.end(), '.', '-');
    return hostNameStr;
  } else {
    return std::string("NO-HOSTNAME");
  }
}

std::optional<std::string> getEnvironmentVariable(const std::string& variableName) {
  char* env = std::getenv(variableName.c_str());
  if (env == nullptr) {
    return std::optional<std::string>();
  } else {
    return std::string(env);
  }
}
}  // namespace sv
