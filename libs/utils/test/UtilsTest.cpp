#include <gtest/gtest.h>

#include "Utils.h"

namespace sv {

TEST(Utils, getIndexOfClosestValueDouble) {
  const std::vector<double> sorted_vec = {1.0, 2.0, 3.0};

  EXPECT_EQ(sv::getIndexOfClosestValue(1.1, sorted_vec), 0);
  EXPECT_EQ(sv::getIndexOfClosestValue(0.9, sorted_vec), 0);
  EXPECT_EQ(sv::getIndexOfClosestValue(1.0, sorted_vec), 0);

  EXPECT_EQ(sv::getIndexOfClosestValue(2.1, sorted_vec), 1);
  EXPECT_EQ(sv::getIndexOfClosestValue(1.9, sorted_vec), 1);
  EXPECT_EQ(sv::getIndexOfClosestValue(2.0, sorted_vec), 1);

  EXPECT_EQ(sv::getIndexOfClosestValue(3.1, sorted_vec), 2);
  EXPECT_EQ(sv::getIndexOfClosestValue(2.9, sorted_vec), 2);
  EXPECT_EQ(sv::getIndexOfClosestValue(3.0, sorted_vec), 2);
}

TEST(Utils, getIndexOfClosestValueInt) {
  const std::vector<int> sorted_vec = {-1, 0, 1, 3};

  EXPECT_EQ(sv::getIndexOfClosestValue(-2, sorted_vec), 0);
  EXPECT_EQ(sv::getIndexOfClosestValue(-1, sorted_vec), 0);
  EXPECT_EQ(sv::getIndexOfClosestValue(0, sorted_vec), 1);
  EXPECT_EQ(sv::getIndexOfClosestValue(1, sorted_vec), 2);
  EXPECT_EQ(sv::getIndexOfClosestValue(2, sorted_vec), 2);
  EXPECT_EQ(sv::getIndexOfClosestValue(3, sorted_vec), 3);
  EXPECT_EQ(sv::getIndexOfClosestValue(4, sorted_vec), 3);
}

TEST(Utils, toStringPrecision) {
  double pi = 3.14159265359;
  std::string result = sv::to_string(pi, 2);
  EXPECT_EQ(result, "3.14");
}

TEST(Utils, getIndexOfClosestValue) {
  // Test value below min edge
  // clang-format off
// https://eww.pass.panasonic.co.jp/pro-av/support/content/guide/DEF/HE50_120_SERIAL/ConvertibleProtocol.pdf
// Values come from documentation, see RD-1121
//
  unsigned int NUM_ELEMENTS_TEST = 100;
  std::vector<double> VELOCITY_MAPPING_TEST =
      {-180.14, -180.14, -175.70, -169.87, -164.13, -158.50, -152.96, -147.53, -142.19, -136.95,
       -131.80, -126.76, -121.81, -116.97, -112.22, -107.57, -103.01,  -98.56,  -93.13,  -87.86,
       -82.74,  -77.77,  -72.95,  -67.38,  -62.03,  -56.90,  -51.19,  -45.04,  -39.28,  -33.92,
       -28.95,  -24.37,  -20.19,  -16.40,  -13.01,  -10.36,   -8.33,   -6.52,   -5.18,   -4.00,
       -2.97,   -2.25,   -1.64,   -1.24,   -0.90,   -0.61,   -0.45,   -0.32,   -0.06,   -0.00,
       0.00,    0.06,    0.32,    0.45,    0.61,    0.90,    1.24,    1.64,    2.25,    2.97,
       4.00,    5.18,    6.52,    8.33,   10.36,   13.01,   16.40,   20.19,   24.37,   28.95,
       33.92,   39.28,   45.04,   51.19,   56.90,   62.03,   67.38,   72.95,   77.77,   82.74,
       87.86,   93.13,   98.56,  103.01,  107.57,  112.22,  116.97,  121.81,  126.76,  131.80,
       136.95,  142.19,  147.53,  152.96,  158.50,  164.13,  169.87,  175.70,  180.14,  180.14};

  // As defined in the document, but we don't need it for the test
//  std::vector<double> VELOCITY_INCREMENTS_TEST =
//    { 1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
//      11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
//      21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
//      31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
//      41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
//      50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
//      60, 61, 62, 63, 64, 65, 66, 67, 68, 69,
//      70, 71, 72, 73, 74, 75, 76, 77, 78, 79,
//      80, 81, 82, 83, 84, 85, 86, 87, 88, 89,
//      90, 91, 92, 93, 94, 95, 96, 97, 98, 99};
  // clang-format on

  EXPECT_EQ(0, getIndexOfClosestValue(-190.0, VELOCITY_MAPPING_TEST));

  // Test min edge
  EXPECT_EQ(1, getIndexOfClosestValue(-180.14, VELOCITY_MAPPING_TEST));

  // Test some values in between
  EXPECT_EQ(1, getIndexOfClosestValue(-180.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(10, getIndexOfClosestValue(-130.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(17, getIndexOfClosestValue(-100.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(25, getIndexOfClosestValue(-55.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(31, getIndexOfClosestValue(-25.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(44, getIndexOfClosestValue(-1.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(49, getIndexOfClosestValue(-0.01, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(50, getIndexOfClosestValue(-0.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(50, getIndexOfClosestValue(0.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(60, getIndexOfClosestValue(4.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(68, getIndexOfClosestValue(26.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(73, getIndexOfClosestValue(50.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(78, getIndexOfClosestValue(80.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(87, getIndexOfClosestValue(120.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(94, getIndexOfClosestValue(160.0, VELOCITY_MAPPING_TEST));
  EXPECT_EQ(98, getIndexOfClosestValue(180.0, VELOCITY_MAPPING_TEST));

  // Test max edge
  EXPECT_EQ(99, getIndexOfClosestValue(180.14, VELOCITY_MAPPING_TEST));

  // Test value above max edge
  EXPECT_EQ(99, getIndexOfClosestValue(190.0, VELOCITY_MAPPING_TEST));
}
}  // namespace sv
