#include <gtest/gtest.h>

#include "ByteOperations.h"

namespace sv {

TEST(BytesOperation, Extract_1) {
  uint8_t first = 0;
  uint8_t second = 0;
  uint16_t extract = combineTwoBytes(first, second);
  EXPECT_EQ(extract, 0);
}

TEST(BytesOperation, Extract_2) {
  uint8_t first = 0x20;
  uint8_t second = 0xff;
  uint16_t extract = combineTwoBytes(first, second);
  EXPECT_EQ(extract, 0x20ff);
}

TEST(BytesOperation, Convert_unit8_t) {
  uint8_t value = 11;
  uint8_t bytes[4];
  convertToByteArray(&bytes[1], value);
  EXPECT_EQ(value, convertFromByteArray<uint8_t>(&bytes[1]));
  convertToByteArray(&bytes[1], value, true);
  EXPECT_EQ(value, convertFromByteArray<uint8_t>(&bytes[1], true));
}

TEST(BytesOperation, Convert_unit16_t) {
  uint16_t value = 11;
  uint8_t bytes[4];
  convertToByteArray(&bytes[1], value);
  EXPECT_EQ(value, convertFromByteArray<uint16_t>(&bytes[1]));
  convertToByteArray(&bytes[1], value, true);
  EXPECT_EQ(value, convertFromByteArray<uint16_t>(&bytes[1], true));
}

TEST(BytesOperation, Convert_unit32_t) {
  uint32_t value = 11;
  uint8_t bytes[6];
  convertToByteArray(&bytes[1], value);
  EXPECT_EQ(value, convertFromByteArray<uint32_t>(&bytes[1]));
  convertToByteArray(&bytes[1], value, true);
  EXPECT_EQ(value, convertFromByteArray<uint32_t>(&bytes[1], true));
}

TEST(BytesOperation, Convert_float) {
  float value = 11.0f;
  uint8_t bytes[6];
  convertToByteArray(&bytes[1], value);
  EXPECT_EQ(value, convertFromByteArray<float>(&bytes[1]));
  convertToByteArray(&bytes[1], value, true);
  EXPECT_EQ(value, convertFromByteArray<float>(&bytes[1], true));
}

TEST(BytesOperation, Convert_double) {
  double value = 11.0;
  uint8_t bytes[18];
  convertToByteArray(&bytes[1], value);
  EXPECT_EQ(value, convertFromByteArray<double>(&bytes[1]));
  convertToByteArray(&bytes[1], value, true);
  EXPECT_EQ(value, convertFromByteArray<double>(&bytes[1], true));
}
TEST(BytesOperation, extractSigned24BitMaxNegativ) {
  uint8_t first_byte = 0xa8;
  uint8_t second_byte = 0x80;
  uint8_t third_byte = 0x00;
  int value = extractSigned24Bit(first_byte, second_byte, third_byte);
  EXPECT_EQ(value, -5734399);
}
TEST(BytesOperation, extractSigned24BitMaxPositiv) {
  uint8_t first_byte = 0x57;
  uint8_t second_byte = 0x80;
  uint8_t third_byte = 0x00;
  int value = extractSigned24Bit(first_byte, second_byte, third_byte);
  EXPECT_EQ(value, 5734400);
}
TEST(BytesOperation, extractSigned24BitPositiv) {
  uint8_t first_byte = 0x3b;
  uint8_t second_byte = 0xc2;
  uint8_t third_byte = 0xd0;
  int value = extractSigned24Bit(first_byte, second_byte, third_byte);
  EXPECT_EQ(value, 3916496);
}
TEST(BytesOperation, extractSigned24BitNegativ) {
  uint8_t first_byte = 0xe5;
  uint8_t second_byte = 0xab;
  uint8_t third_byte = 0x9a;
  int value = extractSigned24Bit(first_byte, second_byte, third_byte);
  EXPECT_EQ(value, -1725541);
}
}  // namespace sv
