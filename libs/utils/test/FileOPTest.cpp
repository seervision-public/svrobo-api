#include <gtest/gtest.h>
#include "Utils.h"

namespace sv {

TEST(FileOP, addPaths) {
  std::string path1 = "/hans/dula/dumms";
  std::string path2 = "ciao////but";
  std::string path3 = "hoi/";

  ASSERT_EQ("/hans/dula/dumms/hoi/ciao////but", addPaths(path1, path3, path2));
}

}  // namespace sv
