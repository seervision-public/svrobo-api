
## Use C++17, print all warnings
# see https://stackoverflow.com/questions/5088460/flags-to-enable-thorough-and-verbose-g-warnings/9862800#9862800
function(get_sv_relaxed_compile_options RESULT)
  set(SV_RELAXED_COMPILE_OPTIONS "-std=c++17;-pedantic;-Wall;-Wextra;-Wcast-align;-Wcast-qual;-Wctor-dtor-privacy;-Wdisabled-optimization;-Wformat=2;-Winit-self;-Wlogical-op;-Wno-parentheses;-Wmissing-declarations;-Wmissing-include-dirs;-Wnoexcept;-Woverloaded-virtual;-Wredundant-decls;-Wsign-promo;-Wstrict-null-sentinel;-Wstrict-overflow=5;-Wswitch-default;-Wundef;-Wno-unused;-Wold-style-cast;-Wshadow;-Wsign-conversion")
  set(${RESULT} ${SV_RELAXED_COMPILE_OPTIONS} PARENT_SCOPE)
endfunction()

function(get_sv_compile_options RESULT)
  get_sv_relaxed_compile_options(SV_COMPILE_OPTIONS)
  set(SV_COMPILE_OPTIONS "${SV_COMPILE_OPTIONS};-Werror")
  set(${RESULT} ${SV_COMPILE_OPTIONS} PARENT_SCOPE)
endfunction()
