#pragma once

#include <cinttypes>

namespace sv::robo {
using FaultType = uint32_t;
// The severity of the faults is encoded in the two most significant bits
static constexpr FaultType ERROR = (0x0 << (8 * sizeof(FaultType) - 18));
static constexpr FaultType CRITICAL = (0x1 << (8 * sizeof(FaultType) - 18));
static constexpr FaultType SEVERE = (0x2 << (8 * sizeof(FaultType) - 18));
static constexpr FaultType FATAL = (0x3 << (8 * sizeof(FaultType) - 18));
static constexpr FaultType FAULT_MASK = ERROR | SEVERE | CRITICAL | FATAL;

static constexpr FaultType toError(FaultType n) noexcept { return (ERROR | ((~FAULT_MASK) & n)); }
static constexpr FaultType toCritical(FaultType n) noexcept { return (CRITICAL | ((~FAULT_MASK) & n)); }
static constexpr FaultType toSevere(FaultType n) noexcept { return (SEVERE | ((~FAULT_MASK) & n)); }
static constexpr FaultType toFatal(FaultType n) noexcept { return (FATAL | ((~FAULT_MASK) & n)); }

enum class Fault : FaultType {
  // Errors
  GENERIC_ERROR = toError(
      0),  //! @todo dominik.walder: verify that the effective values correspond with the one in the specification
  // Criticals
  GENERIC_CRITICAL = toCritical(0),
  // Severes,
  GENERIC_SEVERE = toSevere(0),
  CONNECTION_FAILURE = toSevere(1),
  // Fatals
  GENERIC_FATAL = toFatal(0)
};

inline static constexpr FaultType getSeverity(FaultType fault) { return FAULT_MASK & fault; }

inline static constexpr bool sameSeverity(FaultType actual, FaultType toMatch) {
  return getSeverity(actual) == getSeverity(toMatch);
}

}  // namespace sv::robo
