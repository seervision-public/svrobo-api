#pragma once

#include "enum.h"

//! @note IMPORTANT the values of these enums need to be unique and between 0 and "number of enums"-1
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
BETTER_ENUM(PTUAxisID,
            uint32_t,
            global = 0,
            pan = 1,
            tilt = 2,
            roll = 3,
            zoom = 4,
            focus = 5,
            iris = 6,
            x = 7,
            y = 8,
            z = 9,
            range = 10)
#pragma GCC diagnostic pop
