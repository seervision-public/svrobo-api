#pragma once

#include <functional>
#include <iostream>

#include <gtest/gtest_prod.h>
#include <msgpack.hpp>

#include "ByteOperations.h"
#include "Log.h"
#include "SVRoboPacket.h"

namespace sv::robo {

template<typename Discover,
         typename References,
         typename WriteParameter,
         typename ReadParameter,
         typename StateMachine,
         typename Firmware,
         typename Network>
class PacketHandler {
 public:
  PacketHandler(
      std::function<void(Packet<Discover>)> const& discoverPacketCallBack_,
      std::function<void(Packet<References>)> const& referencePacketCallBack_,
      std::function<void(Packet<WriteParameter>)> const& writeParameterPacketCallBack_,
      std::function<void(Packet<ReadParameter>)> const& readParameterPacketCallBack_,
      std::function<void(Packet<StateMachine>)> const& stateMachinePacketCallBack_,
      std::function<void(Packet<Firmware>)> const& firmwarePacketCallBack_,
      std::function<void(Packet<Network>)> const& networkPacketCallBack_,
      std::function<void(ErrorCode)> const& errorCallback_ = [](robo::ErrorCode) {}) :
      discoverPacketCallBack(discoverPacketCallBack_),
      referencePacketCallBack(referencePacketCallBack_),
      writeParameterPacketCallBack(writeParameterPacketCallBack_),
      readParameterPacketCallBack(readParameterPacketCallBack_),
      stateMachinePacketCallBack(stateMachinePacketCallBack_),
      firmwarePacketCallBack(firmwarePacketCallBack_),
      networkPacketCallBack(networkPacketCallBack_),
      errorCallback(errorCallback_) {}

  void receive(RawPacket const& packet) {
    Header header;
    try {
      if (!isValidSessionId(header.sessionId, packet)) {
        // TODO Do something
        return;
      } else {
        // TODO Do something with header.number
        reconstructPacket(packet);
      }
    } catch (const msgpack::unpack_error& e) {
      LOG_ERROR("failed to unpack packet.", {{"what", std::string(e.what())}})
      errorCallback(robo::ErrorCode::UNPACK);
    } catch (const std::exception& e) {
      LOG_ERROR("Callback failure",
                {{"Packet Type", std::string(header.type._to_string())}, {"what", std::string(e.what())}})
      errorCallback(robo::ErrorCode::GENERAL);
    }
  }

  static Header extractHeader(RawPacket const& rawPacket) {
    auto oh = msgpack::v3::unpack(rawPacket.data(), rawPacket.length(), 0);
    msgpack::v3::object obj = oh.get();
    if (obj.type == msgpack::type::ARRAY) {
      if (obj.via.array.size != 2) {
        // error
        throw msgpack::unpack_error("Message array length is not 2.");
      }
      return obj.via.array.ptr[0].as<Header>();

    } else {
      throw msgpack::unpack_error("Wrong message format.");
    }
  }

 private:
  FRIEND_TEST(RawPacket, Parameters);

  std::function<void(Packet<Discover>)> discoverPacketCallBack;
  std::function<void(Packet<References>)> referencePacketCallBack;
  std::function<void(Packet<WriteParameter>)> writeParameterPacketCallBack;
  std::function<void(Packet<ReadParameter>)> readParameterPacketCallBack;
  std::function<void(Packet<StateMachine>)> stateMachinePacketCallBack;
  std::function<void(Packet<Firmware>)> firmwarePacketCallBack;
  std::function<void(Packet<Network>)> networkPacketCallBack;
  std::function<void(robo::ErrorCode)> errorCallback;

  static bool isValidSessionId(uint32_t sessionId, RawPacket const& packet) { return true; }

  void reconstructPacket(RawPacket const& rawPacket) {
    auto oh = msgpack::v3::unpack(rawPacket.data(), rawPacket.length(), 0);
    msgpack::v3::object obj = oh.get();
    if (obj.type == msgpack::type::ARRAY) {
      if (obj.via.array.size != 2) {
        // error
        throw msgpack::unpack_error("Message array length is not 2.");
      }

      Header header = obj.via.array.ptr[0].as<Header>();

      switch (header.type) {
        case PacketType::DISCOVER: {
          Packet<Discover> packet = extractPacket<Discover>(header, obj.via.array.ptr[1]);
          discoverPacketCallBack(packet);
          break;
        }
        case PacketType::REFERENCES: {
          Packet<References> packet = extractPacket<References>(header, obj.via.array.ptr[1]);
          referencePacketCallBack(packet);
          break;
        }
        case PacketType::STATE_MACHINE: {
          Packet<StateMachine> packet = extractPacket<StateMachine>(header, obj.via.array.ptr[1]);
          stateMachinePacketCallBack(packet);
          break;
        }
        case PacketType::WRITE_PARAMETERS: {
          Packet<WriteParameter> packet = extractPacket<WriteParameter>(header, obj.via.array.ptr[1]);
          writeParameterPacketCallBack(packet);
          break;
        }
        case PacketType::READ_PARAMETERS: {
          Packet<ReadParameter> packet = extractPacket<ReadParameter>(header, obj.via.array.ptr[1]);
          readParameterPacketCallBack(packet);
          break;
        }
        case PacketType::UPDATE: {
          Packet<Firmware> packet = extractPacket<Firmware>(header, obj.via.array.ptr[1]);
          firmwarePacketCallBack(packet);
          break;
        }
        case PacketType::NETWORK: {
          Packet<Network> packet = extractPacket<Network>(header, obj.via.array.ptr[1]);
          networkPacketCallBack(packet);
          break;
        }
        default:
        case PacketType::UNDEFINED: {
          break;
        }
      }
    }
  }

  template<typename T>
  static Packet<T> extractPacket(Header const& header, msgpack::object& msgpackObj) {
    T payload = msgpackObj.as<T>();
    return Packet<T>(header, payload);
  }

  template<typename T>
  static T extractPayload(RawPacket const& rawPacket) {
    auto oh = msgpack::v3::unpack(rawPacket.data(), rawPacket.length(), 0);
    msgpack::v3::object obj = oh.get();
    if (obj.type == msgpack::type::ARRAY) {
      if (obj.via.array.size != 2) {
        // error
        throw msgpack::unpack_error("Message array length is not 2.");
      }
      return obj.via.array.ptr[1].as<T>();

    } else {
      throw msgpack::unpack_error("Wrong message format.");
    }
  }
};

using RequestPacketHandler = PacketHandler<DiscoverRequest,
                                           ReferencesRequest,
                                           WriteParametersRequest,
                                           ReadParametersRequest,
                                           StateMachineRequest,
                                           FirmwareRequest,
                                           NetworkChangeRequest>;

using ResponsePacketHandler = PacketHandler<DiscoverResponse,
                                            ReferencesResponse,
                                            WriteParametersResponse,
                                            ReadParametersResponse,
                                            StateMachineResponse,
                                            FirmwareResponse,
                                            NetworkChangeResponse>;
}  // namespace sv::robo
