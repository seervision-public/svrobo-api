
#include "SVRoboProtocol.h"

namespace sv::robo {

std::optional<StateAction> getStateTowards(AxisState from, AxisState towards) {
  switch (from) {
    case AxisState::DISCONNECTED:
      switch (towards) {
        case AxisState::DISABLED:
        case AxisState::READY:
        case AxisState::RUNNING:
        case AxisState::MANUAL_CALIBRATION:
        case AxisState::AUTO_CALIBRATION:
          return StateAction::DISABLED;
        case AxisState::DISARMED:
        case AxisState::STOPPING:
        case AxisState::DISCONNECTED:
          return std::nullopt;
        default:
          LOG_ERROR(fmt::format("Unknown axis {}", towards))
          return std::nullopt;
      }
    case AxisState::DISARMED:
      return std::nullopt;
    case AxisState::DISABLED:
      switch (towards) {
        case AxisState::READY:
        case AxisState::RUNNING:
        case AxisState::STOPPING:
        case AxisState::AUTO_CALIBRATION:
        case AxisState::MANUAL_CALIBRATION:
          return StateAction::READY;
        case AxisState::DISCONNECTED:
        case AxisState::DISARMED:
        case AxisState::DISABLED:
          return std::nullopt;
        default:
          LOG_ERROR(fmt::format("Unknown axis {}", towards))
          return std::nullopt;
      }

    case AxisState::READY:
      switch (towards) {
        case AxisState::DISCONNECTED:
        case AxisState::DISABLED:
          return StateAction::DISABLED;
        case AxisState::RUNNING:
        case AxisState::STOPPING:
          return StateAction::RUNNING;
        case AxisState::AUTO_CALIBRATION:
          return StateAction::AUTO_CALIBRATION;
        case AxisState::MANUAL_CALIBRATION:
          return StateAction::MANUAL_CALIBRATION;
        case AxisState::DISARMED:
        case AxisState::READY:
          return std::nullopt;
        default:
          LOG_ERROR(fmt::format("Unknown axis {}", towards))
          return std::nullopt;
      }
    case AxisState::RUNNING:
      switch (towards) {
        case AxisState::DISCONNECTED:
        case AxisState::DISABLED:
        case AxisState::READY:
          return StateAction::READY;
        case AxisState::STOPPING:
          return StateAction::STOPPING;
        case AxisState::AUTO_CALIBRATION:
        case AxisState::MANUAL_CALIBRATION:
          return StateAction::READY;
        case AxisState::DISARMED:
        case AxisState::RUNNING:
          return std::nullopt;
        default:
          LOG_ERROR(fmt::format("Unknown axis {}", towards))
          return std::nullopt;
      }
    case AxisState::STOPPING:
      return std::nullopt;
    case AxisState::AUTO_CALIBRATION:
      return std::nullopt;
    case AxisState::MANUAL_CALIBRATION:
      switch (towards) {
        case AxisState::DISCONNECTED:
        case AxisState::DISABLED:
        case AxisState::READY:
        case AxisState::RUNNING:
        case AxisState::STOPPING:
        case AxisState::AUTO_CALIBRATION:
          return StateAction::READY;
        case AxisState::DISARMED:
        case AxisState::MANUAL_CALIBRATION:
          return std::nullopt;
        default:
          LOG_ERROR(fmt::format("Unknown axis {}", towards))
          return std::nullopt;
      }
    default:
      LOG_ERROR(fmt::format("Unknown axis {}", from))
      return std::nullopt;
  }
}

bool checkResponse(const StateMachineResponse& received, const StateMachineRequest& expected) {
  for (const auto& [axis, requestedState] : expected) {
    if (received.count(axis) == 0) {
      return false;
    }
    if (received.at(axis).state != requestedState) {
      return false;
    }
  }
  return true;
}

bool checkResponse(const StateMachineResponse& s, AxisState a) {
  for (const auto& [axis, response] : s) {
    if (response.state != a) {
      return false;
    }
  }
  return true;
}

}  // namespace sv::robo
