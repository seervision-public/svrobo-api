#include "SVRoboPacket.h"
#include "SVRoboProtocol.h"

namespace sv::robo {

RawPacket::RawPacket(char data[MAX_LENGTH], unsigned int numBytes) { buffer.write(data, numBytes); }

unsigned int RawPacket::length() const { return static_cast<unsigned int>(buffer.size()); }

uint8_t RawPacket::operator[](unsigned int index) const { return static_cast<uint8_t>(buffer.data()[index]); }

char const* RawPacket::data() const { return buffer.data(); }

char* RawPacket::data() { return buffer.data(); }

asio::const_buffers_1 RawPacket::getBuffer() const {
  asio::const_buffers_1 asioBuffer = asio::buffer(buffer.data(), length());
  return asioBuffer;
}

}  // namespace sv::robo
