#pragma once

#include <iostream>
#include <limits>
#include <type_traits>
#include <utility>
#include <variant>

#include <msgpack.hpp>

#include "enum.h"

#include "Axes.h"
#include "LogAPI.h"
#include "SVRoboFaults.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"

BETTER_ENUM(ApiType, uint32_t, NOMINAL = 0, LIGHT = 1)

BETTER_ENUM(PacketType,
            uint32_t,
            REFERENCES = 0,
            WRITE_PARAMETERS = 1,
            READ_PARAMETERS = 2,
            STATE_MACHINE = 3,
            DISCOVER = 4,
            NETWORK = 5,
            UPDATE = 16,
            UNDEFINED = 255)

enum class StateAction : uint32_t {
  POLL_STATE = 0,
  DISCONNECTED = 1,
  DISABLED = 2,
  READY = 3,
  RUNNING = 4,
  STOPPING = 5,
  AUTO_CALIBRATION = 6,
  MANUAL_CALIBRATION = 7,
  // RESERVED = 8,
  RESET_FAULT = 9
};

BETTER_ENUM(AxisState,
            uint32_t,
            DISCONNECTED = static_cast<uint32_t>(StateAction::DISCONNECTED),
            DISABLED = static_cast<uint32_t>(StateAction::DISABLED),
            READY = static_cast<uint32_t>(StateAction::READY),
            RUNNING = static_cast<uint32_t>(StateAction::RUNNING),
            STOPPING = static_cast<uint32_t>(StateAction::STOPPING),
            AUTO_CALIBRATION = static_cast<uint32_t>(StateAction::AUTO_CALIBRATION),
            MANUAL_CALIBRATION = static_cast<uint32_t>(StateAction::MANUAL_CALIBRATION),
            DISARMED = 8)

BETTER_ENUM(ParameterStatus, uint32_t, SUCCESS = 0, NON_EXISTING = 1, OUT_OF_RANGE = 2, DENIED = 3)

BETTER_ENUM(
    ReferenceStatus, uint32_t, SUCCESS = 0, UNCHANGED = 1, INVALID = 2, ERROR = 3, NON_EXISTING = 4, WRONG_STATE = 5)

BETTER_ENUM(ValueIdentifier,
            uint32_t,
            ANY = 0,
            POSITION = 1,
            VELOCITY = 2,
            ACCELERATION = 3,
            UNIT_POSITION = 4,
            UNIT_VELOCITY = 5,
            UNIT_ACCELERATION = 6,
            ANGULAR_POSITION = 7,
            ANGULAR_VELOCITY = 8,
            ANGULAR_ACCELERATION = 9,
            CURRENT = 10,
            TORQUE = 11,
            TIMESTAMP = 12)

BETTER_ENUM(ChangeNetworkIdentifier, uint32_t, STATUS = 0, NETWORK_INFO = 1)

enum class PTUParameterID : uint32_t {
  MAJOR_API_VERSION = 0,
  MINOR_API_VERSION = 1,
  API_INCARNATION = 2,
  MAXIMUM_PARAMETERS = 3,
  WATCHDOG_ENABLED = 4,
  WATCHDOG_TIMEOUT = 5,
  LIMIT_MIN = 6,
  LIMIT_MAX = 7,
  IP = 8,
  IP_MASK = 9
};
#pragma GCC diagnostic pop

namespace sv::robo {

enum class ErrorCode {
  UNPACK = 0,
  PARSE = 1,
  GENERAL = 2,
};

using parameter_t = std::variant<bool, unsigned int, int, float, double>;

using ReferenceDataType = float;
using ValueIdentifierType = uint32_t;
using AxisIdentifierType = uint32_t ;
using ReferenceData = std::map<ValueIdentifierType, ReferenceDataType>;
using ReferencesRequest = std::map<AxisIdentifierType, ReferenceData>;

struct NetworkData {
  std::string ip;
  std::string mask;
  std::string mac;
  MSGPACK_DEFINE_ARRAY(ip, mask, mac)
};

struct VersionData {
  uint32_t major;
  uint32_t minor;
  uint32_t apiType;
  MSGPACK_DEFINE_ARRAY(major, minor, apiType)
};

using DiscoverRequest = msgpack::type::nil_t;

struct DiscoverResponseData {
  VersionData version;
  std::vector<NetworkData> network;
  MSGPACK_DEFINE_MAP(version, network)
};

using DiscoverResponse = DiscoverResponseData;

using NetworkChangeRequest = std::map<uint32_t, NetworkData>;

using NetworkDataResponse = std::variant<bool, NetworkData>;
using NetworkChangeResponse = std::map<uint32_t, NetworkDataResponse>;

struct ReferenceResponseEntry {
  ReferenceResponseEntry(){}
  ReferenceResponseEntry(uint32_t status_,
  ReferenceData data_) : status(status_), data(data_) {}
  uint32_t status;
  ReferenceData data;
  MSGPACK_DEFINE(status, data)
};

using ReferencesResponse = std::map<AxisIdentifierType, ReferenceResponseEntry>;

}  // namespace sv::robo

namespace msgpack {
MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
  namespace adaptor {

  template<>
  struct pack<std::monostate> {
    template<typename Stream>
    msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& packer, std::monostate const&) const {
      packer.pack_nil();
      return packer;
    }
  };

  template<typename... Ts>
  struct pack<std::variant<Ts...>> {
    template<typename Stream>
    msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& packer, std::variant<Ts...> const& param) const {
      std::visit([&](auto&& value) { packer.pack(value); }, param);
      return packer;
    }
  };

  template<typename... Ts>
  struct object<std::variant<Ts...>> {
    void operator()(msgpack::object& node, std::variant<Ts...> const& param) const {
      std::visit(
          [&](auto const& value) noexcept {
            using T = std::decay_t<decltype(value)>;
            object<T>()(node, value);
          },
          param);
    }
  };

  template<typename... Ts>
  struct object_with_zone<std::variant<Ts...>> {
    void operator()(msgpack::object::with_zone& node, std::variant<Ts...> const& param) const {
      std::visit(
          [&](auto const& value) noexcept {
            using T = std::decay_t<decltype(value)>;
            object_with_zone<T>()(node, value);
          },
          param);
    }
  };

  template<>
  struct pack<sv::robo::ReferenceResponseEntry> {
    template<typename Stream>
    msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& packer,
                                        sv::robo::ReferenceResponseEntry const& entry) const {
      packer.pack_array(2);
      packer.pack(entry.status);
      packer.pack(entry.data);
      return packer;
    }
  };

  template<class ToType, class FromType>
  void assignIfNotNil(FromType& from, const msgpack::object& node) {
    if (node.type == msgpack::type::NIL) {
      from = ToType();
    } else {
      from = node.as<ToType>();
    }
  }

  template<>
  struct convert<sv::robo::parameter_t> {
    msgpack::object const& operator()(msgpack::object const& node, sv::robo::parameter_t& param) const {
      switch (node.type) {
        case type::BOOLEAN:
          param = bool();
          node.convert(std::get<bool>(param));
          break;
        case type::POSITIVE_INTEGER:
          param = unsigned();
          node.convert(std::get<unsigned int>(param));
          break;
        case type::NEGATIVE_INTEGER:
          param = int();
          node.convert(std::get<int>(param));
          break;
        case type::FLOAT32:
          param = float();
          node.convert(std::get<float>(param));
          break;
        case type::FLOAT64:
          param = double();
          node.convert(std::get<double>(param));
          break;
        default:
          break;
      }
      return node;
    }
  };

  template<>
  struct convert<sv::robo::NetworkDataResponse> {
    msgpack::object const& operator()(msgpack::object const& node, sv::robo::NetworkDataResponse& param) const {
      switch (node.type) {
        case type::BOOLEAN:
          param = bool();
          node.convert(std::get<bool>(param));
          break;
        case type::ARRAY:
          param = sv::robo::NetworkData();
          node.convert(std::get<sv::robo::NetworkData>(param));
          break;
        default:
          break;
      }
      return node;
    }
  };

  }  // namespace adaptor
}
}  // namespace msgpack

namespace sv::robo {

static_assert(std::numeric_limits<float>::has_quiet_NaN, "NOT_ASSIGNED cannot be defined - quiet NaNs do not exist");
static constexpr float NOT_ASSIGNED = std::numeric_limits<float>::quiet_NaN();

/* Data Structures */

static constexpr unsigned int MAX_LENGTH = 508;
// TODO [stefan.frei]: Changing any of the header types does not automatically adapt the available
// header bytes in the buffer
struct HeaderDef {
  static constexpr uint32_t SESSION_POSITION = 0;
  static constexpr uint32_t NUMBER_POSITION = 4;
  static constexpr uint32_t TYPE_POSITION = 8;
  static constexpr uint32_t SESSION_LENGTH = 4;
  static constexpr uint32_t NUMBER_LENGTH = 4;
  static constexpr uint32_t ID_LENGTH = 4;
  static constexpr uint32_t LENGTH = SESSION_LENGTH + NUMBER_LENGTH + ID_LENGTH;
};

struct Header {
  uint32_t sessionId = 0;
  uint32_t number = 0;
  PacketType type = PacketType::UNDEFINED;
  MSGPACK_DEFINE_ARRAY(sessionId, number, const_cast<uint32_t&>(reinterpret_cast<const uint32_t&>(type)))
};

struct Parameter {
  uint32_t id;
  parameter_t value;
  MSGPACK_DEFINE(id, value)
};

// TODO for vector packets, check the length of the vectors (there is a maximum)
struct ParameterStatusMsg {
  uint32_t id = 0;
  ParameterStatus status = ParameterStatus::NON_EXISTING;
  MSGPACK_DEFINE(id, const_cast<uint32_t&>(reinterpret_cast<const uint32_t&>(status)))
};

using Parameters = std::map<uint32_t, parameter_t>;
using ParametersStatus = std::map<uint32_t, uint32_t>;

using WriteParametersRequest = std::map<uint32_t, Parameters>;
using WriteParametersResponse = std::map<uint32_t, ParametersStatus>;

using ReadParametersRequest = std::map<uint32_t, std::vector<uint32_t>>;
using ReadParametersResponse = std::map<uint32_t, Parameters>;

// TODO [stefan.frei] group axis states in one struct, reuse for the response
using StateMachineRequest = std::map<uint32_t, uint32_t>;

struct StateMachineResponseEntry {
  uint32_t state;
  std::vector<uint32_t> faults;
  std::vector<uint32_t> diagnostics;
  MSGPACK_DEFINE(state, faults, diagnostics)
};

using StateMachineResponse = std::map<uint32_t, StateMachineResponseEntry>;

struct FirmwareRequest {
  // TODO [stefan.frei]: Implement
  MSGPACK_DEFINE()
};

struct FirmwareResponse {
  // TODO [stefan.frei]: Implement
  MSGPACK_DEFINE()
};

template<typename Payload>
struct Packet {
 public:
  Header header;
  Payload payload;
  Packet(const Header& header_, const Payload& payload_) : header(header_), payload(payload_) {}
  Packet() : header(), payload() {}
  MSGPACK_DEFINE_ARRAY(header, payload)
};

/* */

/**
 * This function represents and allows to query the possible state transitions that can be
 * requested.
 * @param from State from which we want to transition
 * @param towards State to which we (eventually) want to transition
 * @return State which we have to transition to next to (eventually) reach the State towards. If it
 * is not possible to (eventually) reach "towards" from "from", then std::nullopt is returned
 */
std::optional<StateAction> getStateTowards(AxisState from, AxisState towards);

bool checkResponse(const StateMachineResponse& received, const StateMachineRequest& expected);
bool checkResponse(const StateMachineResponse& s, AxisState a);

template<class Payload>
PacketType getPacketType(const Payload& payload) {
  if (std::is_same<Payload, DiscoverRequest>::value || std::is_same<Payload, DiscoverResponse>::value) {
    return PacketType::DISCOVER;
  } else if (std::is_same<Payload, ReferencesRequest>::value || std::is_same<Payload, ReferencesResponse>::value) {
    return PacketType::REFERENCES;
  } else if (std::is_same<Payload, WriteParametersRequest>::value ||
             std::is_same<Payload, WriteParametersResponse>::value) {
    return PacketType::WRITE_PARAMETERS;
  } else if (std::is_same<Payload, ReadParametersRequest>::value ||
             std::is_same<Payload, ReadParametersResponse>::value) {
    return PacketType::READ_PARAMETERS;
  } else if (std::is_same<Payload, StateMachineRequest>::value || std::is_same<Payload, StateMachineResponse>::value) {
    return PacketType::STATE_MACHINE;
  } else if (std::is_same<Payload, FirmwareRequest>::value || std::is_same<Payload, FirmwareResponse>::value) {
    return PacketType::UPDATE;
  } else if (std::is_same<Payload, NetworkChangeRequest>::value ||
             std::is_same<Payload, NetworkChangeResponse>::value) {
    return PacketType::NETWORK;
  } else {
    //! @todo [damian.frick] we should fail here
    return PacketType::UNDEFINED;
  }
}

/* Printing functions */
template<typename Stream>
Stream& operator<<(Stream& stream, const Header& header) {
  stream << "session id = " << header.sessionId << "\n";
  stream << "packet number = " << header.number << "\n";
  stream << "packet type = " << static_cast<int>(header.type);
  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const ReferencesRequest& request) {
  for (const auto& [axis, data] : request) {
    stream << PTUAxisID::_from_integral(axis) << ": " << data << "\n";
  }
  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const ReferenceData& request) {
  for (const auto& [valueId, data] : request) {
    stream << "\t" << ValueIdentifier::_from_integral(valueId) << ": " << data << "\n";
  }
  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const ReferencesResponse& response) {
  for (const auto& [axis, entry] : response) {
    stream << PTUAxisID::_from_integral(axis) << ": " << ReferenceStatus::_from_integral(entry.status) << "\n"
           << entry.data;
  }
  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const Parameter& parameter) {
  stream << "id: " << parameter.id;
  std::visit([&](auto&& arg) { stream << ", value: " << arg; }, parameter.value);
  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const Parameters& parameters) {
  for (const auto& [id, value] : parameters) {
    stream << "\n\tid: " << id;
    std::visit([&](auto&& arg) { stream << ", value: " << arg; }, value);
  }

  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const WriteParametersRequest& writeParametersRequest) {
  for (auto const& [axis, parameters] : writeParametersRequest) {
    stream << PTUAxisID::_from_integral(axis) << ": ";
    stream << parameters;
    stream << "\n";
  }
  stream << "\n";
  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const ParameterStatusMsg& parameterStatus) {
  stream << "id: " << parameterStatus.id << ", status: " << static_cast<int>(parameterStatus.status);
  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const WriteParametersResponse& writeParametersResponse) {
  for (auto const& [axis, parametersStatus] : writeParametersResponse) {
    stream << PTUAxisID::_from_integral(axis) << ": ";
    stream << parametersStatus;
    stream << "\n";
  }
  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const ReadParametersRequest& readParametersRequest) {
  for (auto const& [axis, parameters] : readParametersRequest) {
    stream << axis;
    for (auto const& parameter : parameters) {
      stream << "\n\t" << parameter;
    }
  }
  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const StateMachineRequest& o) {
  for (const auto& [axis, state] : o) {
    stream << PTUAxisID::_from_integral(axis) << ": " << AxisState::_from_integral(state) << " state\n";
  }
  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const StateMachineResponse& o) {
  for (const auto& [axis, response] : o) {
    stream << PTUAxisID::_from_integral(axis) << ": " << AxisState::_from_integral(response.state);
    if (!response.faults.empty()) {
      stream << "| faults: ";
      for (const auto& fault : response.faults) {
        stream << fault << " ";
      }
    }
    if (!response.diagnostics.empty()) {
      stream << "| diagnostics: ";
      for (const auto& diagnostic : response.diagnostics) {
        stream << diagnostic << " ";
      }
    }
    stream << "\n";
  }
  return stream;
}

template<typename Stream, typename Payload>
Stream& operator<<(Stream& stream, const Packet<Payload>& packet) {
  stream << "#Packet header" << '\n' << packet.header << '\n' << "#Packet payload" << '\n' << packet.payload;
  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const NetworkData& networkData) {
  stream << "\tIP:" << networkData.ip << "\tMASK:" << networkData.mask << "\tMAC:" << networkData.mac << "\n";
  return stream;
}

// template<typename Stream>
// Stream& operator<<(Stream& stream, const NetworkDataResponse& networkDataResponse) {
//   stream << "status:" << networkDataResponse.status << "\n" << networkDataResponse.net;
//   return stream;
// }

template<typename Stream>
Stream& operator<<(Stream& stream, const DiscoverResponse& discoverResponse) {
  // for(const auto& [id, data] : discoverResponse) {
  stream << "\nVERSION:\t" << discoverResponse.version.major << "." << discoverResponse.version.minor;
  stream << "\nAPI:\t" << ApiType::_from_integral(discoverResponse.version.apiType);
  stream << "\nNETWORK DATA: ";
  for (const auto& netData : discoverResponse.network) {
    stream << netData;
  }

  return stream;
}

}  // namespace sv::robo
