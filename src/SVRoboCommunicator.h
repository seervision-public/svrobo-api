#pragma once

#include <asio/ts/buffer.hpp>
#include <asio/ts/internet.hpp>
#include <functional>
#include <future>
#include <iostream>

#include "LogAPI.h"
#include "SVRoboPacket.h"
#include "SVRoboPacketHandler.h"

/**
 * This file implements three classes for communication with the robot using UDP.
 *
 * SVRoboCommunicator
 * SVRoboServer
 * SVRoboClient
 *
 * SVRoboClient and Server both have an instance of SVRoboCommunicator class where the communication is handled
 * using ASIO library. Depending on the role being the server or client, the communication has different needs.
 *
 * The main difference between Server and Client class is
 * - respond function of SVRoboCommunicator which is only called in Server class.
 * - setPort for server and setAddressAndPort for client.
 *
 * Currently SVRoboServer is used by ptu_firmware and
 * SVRoboClient by ptu_driver
 *
 * */
namespace sv::robo {

template<typename Discover,
         typename References,
         typename WriteParameter,
         typename ReadParameter,
         typename StateMachine,
         typename Firmware,
         typename Network>
class SVRoboCommunicator {
 public:
  // Client Ctor
  SVRoboCommunicator(std::string const& address,
                     unsigned short const& port,
                     std::function<void(Packet<DiscoverResponse>)> const& discoverPacketCallBack,
                     std::function<void(Packet<ReferencesResponse>)> const& referencePacketCallBack,
                     std::function<void(Packet<WriteParametersResponse>)> const& writeParameterPacketCallBack,
                     std::function<void(Packet<ReadParametersResponse>)> const& readParameterPacketCallBack,
                     std::function<void(Packet<StateMachineResponse>)> const& stateMachinePacketCallBack,
                     std::function<void(Packet<FirmwareResponse>)> const& firmwarePacketCallBack,
                     std::function<void(Packet<NetworkChangeResponse>)> const& networkPacketCallBack,
                     std::function<void(ErrorCode)> const& errorHandler) :
      SVRoboCommunicator(discoverPacketCallBack,
                         referencePacketCallBack,
                         writeParameterPacketCallBack,
                         readParameterPacketCallBack,
                         stateMachinePacketCallBack,
                         firmwarePacketCallBack,
                         networkPacketCallBack,
                         errorHandler) {
    setAddressAndPort(address, port);
  }
  // Client Ctor
  SVRoboCommunicator(std::function<void(Packet<DiscoverResponse>)> const& discoverPacketCallBack,
                     std::function<void(Packet<ReferencesResponse>)> const& referencePacketCallBack,
                     std::function<void(Packet<WriteParametersResponse>)> const& writeParameterPacketCallBack,
                     std::function<void(Packet<ReadParametersResponse>)> const& readParameterPacketCallBack,
                     std::function<void(Packet<StateMachineResponse>)> const& stateMachinePacketCallBack,
                     std::function<void(Packet<FirmwareResponse>)> const& firmwarePacketCallBack,
                     std::function<void(Packet<NetworkChangeResponse>)> const& networkPacketCallBack,
                     std::function<void(ErrorCode)> const& errorHandler) :
      io_context_(),
      socket(new asio::ip::udp::socket(io_context_, asio::ip::udp::endpoint(asio::ip::udp::v4(), 0))),
      resolver_(io_context_),
      endpoint_(),
      handler_(discoverPacketCallBack,
               referencePacketCallBack,
               writeParameterPacketCallBack,
               readParameterPacketCallBack,
               stateMachinePacketCallBack,
               firmwarePacketCallBack,
               networkPacketCallBack,
               errorHandler),
      runThread_() {}

  // Server Ctor
  SVRoboCommunicator(unsigned short const& port,
                     std::function<void(Packet<DiscoverRequest>)> const& discoverPacketCallBack,
                     std::function<void(Packet<ReferencesRequest>)> const& referencePacketCallBack,
                     std::function<void(Packet<WriteParametersRequest>)> const& writeParameterPacketCallBack,
                     std::function<void(Packet<ReadParametersRequest>)> const& readParameterPacketCallBack,
                     std::function<void(Packet<StateMachineRequest>)> const& stateMachinePacketCallBack,
                     std::function<void(Packet<FirmwareRequest>)> const& firmwarePacketCallBack,
                     std::function<void(Packet<NetworkChangeRequest>)> const& networkPacketCallBack,
                     std::function<void(ErrorCode)> const& errorHandler) :
      io_context_(),
      socket(new asio::ip::udp::socket(io_context_, asio::ip::udp::endpoint(asio::ip::udp::v4(), port))),
      resolver_(io_context_),
      endpoint_(),
      handler_(discoverPacketCallBack,
               referencePacketCallBack,
               writeParameterPacketCallBack,
               readParameterPacketCallBack,
               stateMachinePacketCallBack,
               firmwarePacketCallBack,
               networkPacketCallBack,
               errorHandler),
      runThread_() {}

  // Server Ctor
  SVRoboCommunicator(unsigned short const& port) : SVRoboCommunicator(port, {}, {}, {}, {}, {}, {}, {}, {}) {}

  ~SVRoboCommunicator() { stop(); }
  void send(const RawPacket& packet) {
    if (stopCommand_) {  // Cannot send if stopped
      return;
    }
    auto buffer = packet.getBuffer();
    //    LOG_DEBUG("Sending packet");
    socket->send_to(buffer, endpoint_);
  }

  template<class Packet, class Payload>
  void respond(Packet const& packet, Payload&& payload) {
    send(RawPacket(packet.header, payload));
  }

  //! @todo [damian.frick] Return a boolean
  void start() {
    if (!stopCommand_) {  // Only start once
      return;
    }
    stopCommand_ = false;
    LOG_DEBUG("SVRoboDriver: Starting.",
              {
                  {"IP address", endpoint_.address().to_string()},
                  {"Port", endpoint_.port()},
              });

    receive();
    // Note: start io_context_.run() after one handler is registered, because it returns if no handler is registered
    runThread_ = std::thread([this] { io_context_.run(); });
  }

  // Use for client
  void setAddressAndPort(std::string const& address, unsigned short const& port) {
    endpoint_ =
        *resolver_.resolve(asio::ip::udp::v4(), address, std::to_string(static_cast<unsigned int>(port))).begin();
  }

  // Use for server
  void setPort(unsigned short port) {
    auto endpoint = asio::ip::udp::endpoint(asio::ip::udp::v4(), port);
    socket.reset(new asio::ip::udp::socket(io_context_, endpoint));
  }

  void stop() {
    if (stopCommand_) {  // Only stop once
      return;
    }
    LOG_DEBUG("SVRoboDriver: Stopping.");
    stopCommand_ = true;
    socket->cancel();
    socket->close();
    io_context_.stop();
    runThread_.join();
    LOG_DEBUG("SVRoboDriver: Stopped.");
  }

 private:
  static constexpr unsigned int MAX_LENGTH = 508;

  asio::io_context io_context_;
  std::unique_ptr<asio::ip::udp::socket> socket;
  asio::ip::udp::resolver resolver_;
  asio::ip::udp::endpoint endpoint_;
  char data_[MAX_LENGTH];
  PacketHandler<Discover, References, WriteParameter, ReadParameter, StateMachine, Firmware, Network> handler_;
  bool stopCommand_ = true;
  std::thread runThread_;

  //   Note: async_receive_from spawns a new task in a separate thread. The function itself returns immediately.
  void receive() {
    if (stopCommand_) {
      return;
    }
    socket->async_receive_from(
        asio::buffer(data_, MAX_LENGTH), endpoint_, [this](std::error_code ec, std::size_t bytesReceived) {
          handleReceivedBuffer(ec, bytesReceived);
        });
  }
  void handleReceivedBuffer(std::error_code ec, std::size_t bytesReceived) {
    if (!ec && bytesReceived > 0) {
      RawPacket packet(data_, static_cast<unsigned int>(bytesReceived));
      // TODO [stefan.frei] If a queue for received packets is needed, it should be added here
      handler_.receive(packet);
    } else if (ec == asio::error::operation_aborted) {
      LOG_DEBUG("SVRoboDriver: Abort signal received", {});
    } else {
      LOG_DEBUG(fmt::format("SVRoboDriver: Error while receiving. Error code = {}", ec));
    }
    receive();
  }
};

class SVRoboServer {
 public:
  SVRoboServer(unsigned short const& port,
               std::function<void(Packet<DiscoverRequest>)> const& discoverPacketCallBack,
               std::function<void(Packet<ReferencesRequest>)> const& referencePacketCallBack,
               std::function<void(Packet<WriteParametersRequest>)> const& writeParameterPacketCallBack,
               std::function<void(Packet<ReadParametersRequest>)> const& readParameterPacketCallBack,
               std::function<void(Packet<StateMachineRequest>)> const& stateMachinePacketCallBack,
               std::function<void(Packet<FirmwareRequest>)> const& firmwarePacketCallBack,
               std::function<void(Packet<NetworkChangeRequest>)> const& networkPacketCallBack,
               std::function<void(ErrorCode)> const& errorHandler) :
      communicator(port,
                   discoverPacketCallBack,
                   referencePacketCallBack,
                   writeParameterPacketCallBack,
                   readParameterPacketCallBack,
                   stateMachinePacketCallBack,
                   firmwarePacketCallBack,
                   networkPacketCallBack,
                   errorHandler) {}

  SVRoboServer(unsigned short const& port) : communicator(port) {}

  void send(const RawPacket& packet) { communicator.send(packet); }
  template<class Packet, class Payload>
  void respond(Packet const& packet, Payload&& payload) {
    communicator.respond(packet, payload);
  }
  void start() { communicator.start(); }
  void setPort(unsigned short port) { communicator.setPort(port); }
  void stop() { communicator.stop(); }

 private:
  SVRoboCommunicator<DiscoverRequest,
                     ReferencesRequest,
                     WriteParametersRequest,
                     ReadParametersRequest,
                     StateMachineRequest,
                     FirmwareRequest,
                     NetworkChangeRequest>
      communicator;
};

class SVRoboClient {
 public:
  SVRoboClient(std::string const& address,
               unsigned short const& port,
               std::function<void(Packet<DiscoverResponse>)> const& discoverPacketCallBack,
               std::function<void(Packet<ReferencesResponse>)> const& referencePacketCallBack,
               std::function<void(Packet<WriteParametersResponse>)> const& writeParameterPacketCallBack,
               std::function<void(Packet<ReadParametersResponse>)> const& readParameterPacketCallBack,
               std::function<void(Packet<StateMachineResponse>)> const& stateMachinePacketCallBack,
               std::function<void(Packet<FirmwareResponse>)> const& firmwarePacketCallBack,
               std::function<void(Packet<NetworkChangeResponse>)> const& networkPacketCallBack,
               std::function<void(ErrorCode)> const& errorHandler) :
      communicator(address,
                   port,
                   discoverPacketCallBack,
                   referencePacketCallBack,
                   writeParameterPacketCallBack,
                   readParameterPacketCallBack,
                   stateMachinePacketCallBack,
                   firmwarePacketCallBack,
                   networkPacketCallBack,
                   errorHandler) {}

  SVRoboClient(std::function<void(Packet<DiscoverResponse>)> const& discoverPacketCallBack,
               std::function<void(Packet<ReferencesResponse>)> const& referencePacketCallBack,
               std::function<void(Packet<WriteParametersResponse>)> const& writeParameterPacketCallBack,
               std::function<void(Packet<ReadParametersResponse>)> const& readParameterPacketCallBack,
               std::function<void(Packet<StateMachineResponse>)> const& stateMachinePacketCallBack,
               std::function<void(Packet<FirmwareResponse>)> const& firmwarePacketCallBack,
               std::function<void(Packet<NetworkChangeResponse>)> const& networkPacketCallBack,
               std::function<void(ErrorCode)> const& errorHandler) :
      communicator(discoverPacketCallBack,
                   referencePacketCallBack,
                   writeParameterPacketCallBack,
                   readParameterPacketCallBack,
                   stateMachinePacketCallBack,
                   firmwarePacketCallBack,
                   networkPacketCallBack,
                   errorHandler) {}

  void send(const RawPacket& packet) { communicator.send(packet); }
  void start() { communicator.start(); }
  void setAddressAndPort(std::string const& address, unsigned short const& port) {
    communicator.setAddressAndPort(address, port);
  }

  void stop() { communicator.stop(); }

 private:
  SVRoboCommunicator<DiscoverResponse,
                     ReferencesResponse,
                     WriteParametersResponse,
                     ReadParametersResponse,
                     StateMachineResponse,
                     FirmwareResponse,
                     NetworkChangeResponse>
      communicator;
};
}  // namespace sv::robo
