#pragma once

#include <iostream>

#include <asio/ts/buffer.hpp>
#include <msgpack.hpp>

#include "SVRoboProtocol.h"

namespace sv::robo {

class RawPacket {
 public:
  RawPacket() : buffer() {}
  RawPacket(RawPacket&& other) : buffer(std::move(other.buffer)) {}
  template<class Payload>
  RawPacket(const Header& header, const Payload& payload) {
    Packet<Payload> packet;
    packet.header = header;
    packet.payload = payload;

    msgpack::v1::pack(buffer, packet);
  }

  RawPacket(char data[MAX_LENGTH], unsigned int numBytes);

  RawPacket& operator=(RawPacket&& other) {
    if (this != &other) {
      buffer = std::move(other.buffer);
    }
    return *this;
  }

  unsigned int length() const;
  uint8_t operator[](unsigned int index) const;
  char const* data() const;
  char* data();
  asio::const_buffers_1 getBuffer() const;

 private:
  msgpack::v3::sbuffer buffer;
};

template<typename Stream>
Stream& operator<<(Stream& stream, const RawPacket& packet) {
  for (unsigned int i = 0; i < packet.length(); ++i) {
    stream << "Byte " << std::dec << i << ": " << std::hex << (packet[i] & 0xff) << std::dec;
  }
  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const asio::const_buffers_1& buffer) {
  unsigned char const* pBuffer = asio::buffer_cast<unsigned char const*>(buffer);  // For random access
  std::size_t bufferSize = asio::buffer_size(buffer);
  for (int i = 0; i < bufferSize; ++i) {
    stream << std::dec << i << ": " << std::hex << (pBuffer[i] & 0xff) << std::dec;
  }
  return stream;
}

template<typename Stream>
Stream& operator<<(Stream& stream, const asio::mutable_buffers_1& buffer) {
  unsigned char const* pBuffer = asio::buffer_cast<unsigned char const*>(buffer);  // For random access
  std::size_t bufferSize = asio::buffer_size(buffer);
  for (int i = 0; i < bufferSize; ++i) {
    stream << std::dec << i << ": " << std::hex << (pBuffer[i] & 0xff) << std::dec;
  }
  return stream;
}

}  // namespace sv::robo
