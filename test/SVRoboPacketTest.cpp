#include <iostream>

#include <asio/ts/buffer.hpp>

#include <gtest/gtest.h>

#include "SVRoboPacket.h"
#include "SVRoboPacketHandler.h"

namespace sv::robo {

TEST(RawPacket, WriteHeader) {
  constexpr uint32_t packetNumber = 56497;
  constexpr uint32_t sessionId = 150;
  Header headerIn{sessionId, packetNumber, PacketType::REFERENCES};
  ReferencesRequest payload;

  RawPacket packet(headerIn, payload);
  Header header = RequestPacketHandler::extractHeader(packet);
  EXPECT_EQ(header.type, +PacketType::REFERENCES);
  EXPECT_EQ(header.number, packetNumber);
  EXPECT_EQ(header.sessionId, sessionId);
}

TEST(RawPacket, Buffer) {
  constexpr uint32_t packetNumber = 100;
  constexpr uint32_t sessionId = 1;
  Header headerIn{sessionId, packetNumber, PacketType::WRITE_PARAMETERS};
  WriteParametersRequest payload;
  // payload.insert({0, Parameters()});
  payload[0].insert({1, 4});
  payload[0].insert({4, 40.8f});
  payload[0].insert({7, false});
  payload[0].insert({5, -3});
  payload[0].insert({52449, -33.368617});

  RawPacket packet(headerIn, payload);
  ASSERT_EQ(packet.length(), 32);  // @todo [damian.frick] check if correct
  auto buffer = packet.getBuffer();

  auto pBuffer = asio::buffer_cast<unsigned char const*>(buffer);  // For random access
  std::size_t bufferSize = asio::buffer_size(buffer);
  ASSERT_EQ(packet.length(), bufferSize);

  for (unsigned int i = 0; i < packet.length(); ++i) {
    EXPECT_EQ(packet[i], (pBuffer[i] & 0xff)) << " at " << i;
  }
}

TEST(RawPacket, Movable) {
  constexpr uint32_t packetNumber = 100;
  constexpr uint32_t sessionId = 1;
  Header headerIn{sessionId, packetNumber, PacketType::WRITE_PARAMETERS};
  WriteParametersRequest payload;
  // payload.insert({PTUAxisID::pan, std::vector<Parameter>()});
  payload[PTUAxisID::pan].insert({1, 4});
  payload[PTUAxisID::pan].insert({4, 40.8f});
  payload[PTUAxisID::pan].insert({7, false});
  payload[PTUAxisID::pan].insert({5, -3});
  payload[PTUAxisID::pan].insert({52449, -33.368617});
  RawPacket p1(headerIn, payload);

  auto length = p1.length();
  EXPECT_EQ(p1.length(), 32);  // @todo [damian.frick] check if correct
  RawPacket p2(std::move(p1));
  EXPECT_EQ(p2.length(), length);
  EXPECT_EQ(p1.length(), 0);

  RawPacket p3 = std::move(p2);
  EXPECT_EQ(p3.length(), length);
  EXPECT_EQ(p2.length(), 0);
}

template<class T>
struct always_false : std::false_type {};

TEST(RawPacket, Parameters) {
  constexpr uint32_t packetNumber = 100;
  constexpr uint32_t sessionId = 1;
  Header headerIn{sessionId, packetNumber, PacketType::WRITE_PARAMETERS};
  WriteParametersRequest payload;
  // payload.insert({PTUAxisID::pan, std::vector<Parameter>()});
  payload[PTUAxisID::pan].insert({1, 4});
  payload[PTUAxisID::pan].insert({4, 40.8f});
  payload[PTUAxisID::pan].insert({7, false});
  payload[PTUAxisID::pan].insert({5, -3});
  payload[PTUAxisID::pan].insert({52449, -33.368617});

  RawPacket packet(headerIn, payload);
  auto unpackedPayload = RequestPacketHandler::extractPayload<WriteParametersRequest>(packet);

  // Check that ids match
  EXPECT_EQ(payload.size(), unpackedPayload.size());
  for (const auto& [id, value] : payload[PTUAxisID::pan]) {
    bool status = false;
    if (unpackedPayload[PTUAxisID::pan].find(id) != unpackedPayload[PTUAxisID::pan].end()) {
      status = true;
    }
    EXPECT_TRUE(status);
  }

  // Check that values match
  auto checkValue = [&](size_t i, auto value) {
    auto checkMatch = [value](auto&& arg, auto v) noexcept -> bool {
      using T = std::decay_t<decltype(v)>;
      if constexpr (std::is_same_v<T, bool>)
        return (v == arg);
      else if constexpr (std::is_same_v<T, unsigned int>)
        return (v == arg);
      else if constexpr (std::is_same_v<T, int>)
        return (v == static_cast<int>(arg));  // Casting to int to avoid warning when arg is int but positive and thus
                                              // gets interpreted as unsigned int
      else if constexpr (std::is_same_v<T, float>)
        return (v == arg);
      else if constexpr (std::is_same_v<T, double>)
        return (v == arg);
      else
        static_assert(always_false<T>::value, "non-exhaustive visitor!");
    };
    bool packedValueMatches = false;
    std::visit([&](auto&& arg) noexcept { packedValueMatches = checkMatch(arg, value); },
               payload[PTUAxisID::pan].at(i));
    EXPECT_TRUE(packedValueMatches);
    bool unpackedValueMatches = false;
    std::visit([&](auto&& arg) noexcept { unpackedValueMatches = checkMatch(arg, value); },
               unpackedPayload[PTUAxisID::pan].at(i));
    EXPECT_TRUE(unpackedValueMatches);
  };
  checkValue(1, 4);
  checkValue(4, 40.8f);
  checkValue(7, false);
  checkValue(5, -3);
  checkValue(52449, -33.368617);
}

}  // namespace sv::robo
