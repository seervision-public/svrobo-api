#include <gtest/gtest.h>

#include "LogAPI.h"
#include "SignalHandler.h"

int main(int argc, char** argv) {
  sv::log::ConsoleLogger.setup(sv::log::Level::trace);
  testing::InitGoogleTest(&argc, argv);
  sv::signal_handling::initDefaultSignalHandler();
  return RUN_ALL_TESTS();
}
