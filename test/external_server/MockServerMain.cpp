#include <stdio.h>
#include <chrono>
#include <thread>

#include "LogAPI.h"
#include "external_server/MockServer.h"

int main(int argc, char* argv[]) {
  sv::log::ConsoleLogger.setup(sv::log::Level::trace);

  unsigned short port = 0;
  try {
    if (argc != 2) {
      LOG_ERROR("Usage: async_udp_echo_server <port>");
      return 1;
    }
    port = static_cast<unsigned short>(std::stoi(argv[1]));

  } catch (std::exception& e) {
    LOG_ERROR("", {{"Exception", e.what()}});
  }
  LOG_INFO("Press 'any key' + enter to exit.");
  sv::robo::MockServer server(port);
  server.start();

  using namespace std::chrono_literals;
  while (true) {
    int c = getchar();
    if (c != 0) {
      break;
    }
    std::this_thread::sleep_for(5ms);
  }

  return 0;
}
