#pragma once

#include <algorithm>
#include <map>
#include <random>

#include "Axes.h"
#include "LogAPI.h"
#include "SVRoboCommunicator.h"
#include "SVRoboPacket.h"
#include "SVRoboProtocol.h"

namespace sv::robo {

class MockServer {
 public:
  MockServer(unsigned short port, std::uint_fast32_t seed = 0, double p = 0.3) :
      server(
          port,
          [this](Packet<DiscoverRequest> request) { versionCallback(request); },
          [this](Packet<ReferencesRequest> request) { referenceCallback(request); },
          [this](Packet<WriteParametersRequest> request) { writeParameterCallback(request); },
          [this](Packet<ReadParametersRequest> request) { readParameterCallback(request); },
          [this](Packet<StateMachineRequest> request) { stateChangeCallback(request); },
          [this](Packet<FirmwareRequest> request) {},
          [this](Packet<NetworkChangeRequest> request) { networkChangeCallback(request); },
          [](ErrorCode) {}),
      states{{PTUAxisID::pan, AxisState::DISCONNECTED},
             {PTUAxisID::tilt, AxisState::DISCONNECTED},
             {PTUAxisID::zoom, AxisState::DISCONNECTED},
             {PTUAxisID::focus, AxisState::DISCONNECTED},
             {PTUAxisID::iris, AxisState::DISCONNECTED},
             {PTUAxisID::range, AxisState::DISCONNECTED}},
      randomEngine(seed),
      packetSuccessProbability(p) {}

  void start() {
    server.start();
    initialRequest = true;
  }

  void stop() { server.stop(); }

  void setPort(unsigned short port) { server.setPort(port); }
  // TODO [stefan.frei] either fix or remove
  //  void setAddressAndPort(std::string const & address, std::string const & port) {
  //    server.setAddressAndPort(address, port);
  //  }

 private:
  SVRoboServer server;
  bool initialRequest = true;
  std::map<PTUAxisID, uint8_t> states;
  std::mt19937 randomEngine;
  std::uniform_real_distribution<> distribution;
  double packetSuccessProbability = 1.0;
  static constexpr unsigned int VERSION_MAJOR = 1;
  static constexpr unsigned int VERSION_MINOR = 7;
  static constexpr unsigned int VERSION_PATCH = 23;

  void versionCallback(Packet<DiscoverRequest> const& request) {
    DiscoverResponse response;

    response.version.major = VERSION_MAJOR;
    response.version.minor = VERSION_MINOR;
    response.version.apiType = (+ApiType::NOMINAL)._to_integral();

    robo::NetworkData networkData;
    networkData.ip = "0.0.0.0";
    networkData.mask = "255.255.255.0";
    networkData.mac = "aa:bb:cc:dd:ee:ff";

    response.network.push_back(networkData);
    networkData.ip = "127.0.0.1";
    response.network.push_back(networkData);

    server.respond(request, response);
  }

  void referenceCallback(Packet<ReferencesRequest> const& request) {
    LOG_INFO("Process Reference Request");
    LOG_DEBUG(fmt::format("Request:\n{}", request));

    ReferencesResponse response;
    if (initialRequest) {
      response[PTUAxisID::pan].data = {
          {ValueIdentifier::ANGULAR_POSITION, 0.0f},
          {ValueIdentifier::ANGULAR_VELOCITY, 0.0f},
          {ValueIdentifier::CURRENT, 0.0f},
      };
      response[PTUAxisID::tilt].data = {
          {ValueIdentifier::ANGULAR_POSITION, 0.0f},
          {ValueIdentifier::ANGULAR_VELOCITY, 0.0f},
          {ValueIdentifier::CURRENT, 0.0f},
      };
      response[PTUAxisID::zoom].data[ValueIdentifier::UNIT_POSITION] = 0.0f;
      response[PTUAxisID::focus].data[ValueIdentifier::UNIT_POSITION] = 0.0f;
      response[PTUAxisID::iris].data[ValueIdentifier::UNIT_POSITION] = 0.0f;
      response[PTUAxisID::range].data = {{ValueIdentifier::POSITION, 0.0f}, {ValueIdentifier::TIMESTAMP, 0.0f}};
      initialRequest = false;
    }

    response[PTUAxisID::pan].status = ReferenceStatus::SUCCESS;
    response[PTUAxisID::tilt].status = ReferenceStatus::SUCCESS;
    response[PTUAxisID::zoom].status = ReferenceStatus::SUCCESS;
    response[PTUAxisID::focus].status = ReferenceStatus::SUCCESS;
    response[PTUAxisID::iris].status = ReferenceStatus::SUCCESS;
    response[PTUAxisID::range].status = ReferenceStatus::SUCCESS;

    response[PTUAxisID::pan].data = {
        {ValueIdentifier::ANGULAR_POSITION, 0.0f},
        {ValueIdentifier::ANGULAR_VELOCITY, request.payload.at(PTUAxisID::pan).at(ValueIdentifier::ANGULAR_VELOCITY)},
        {ValueIdentifier::CURRENT, 0.0f},
    };
    response[PTUAxisID::tilt].data = {
        {ValueIdentifier::ANGULAR_POSITION, 0.0f},
        {ValueIdentifier::ANGULAR_VELOCITY, request.payload.at(PTUAxisID::tilt).at(ValueIdentifier::ANGULAR_VELOCITY)},
        {ValueIdentifier::CURRENT, 0.0f},
    };

    response[PTUAxisID::zoom].data[ValueIdentifier::UNIT_POSITION] =
        request.payload.at(PTUAxisID::zoom).at(ValueIdentifier::UNIT_POSITION);
    response[PTUAxisID::focus].data[ValueIdentifier::UNIT_POSITION] =
        request.payload.at(PTUAxisID::focus).at(ValueIdentifier::UNIT_POSITION);
    response[PTUAxisID::iris].data[ValueIdentifier::UNIT_POSITION] =
        request.payload.at(PTUAxisID::iris).at(ValueIdentifier::UNIT_POSITION);
    response[PTUAxisID::range].data = {{ValueIdentifier::POSITION, 0.0f}, {ValueIdentifier::TIMESTAMP, 0.0f}};
    server.respond(request, response);
  }

  void stateChangeCallback(Packet<StateMachineRequest> const& request) {
    LOG_INFO("Process State Machine Request");
    LOG_DEBUG(fmt::format("Request:\n{}", request));
    StateMachineResponse response;

    for (const auto& [axis, state] : request.payload) {
      PTUAxisID axisID = PTUAxisID::_from_integral(axis);
      if (distribution(randomEngine) <= packetSuccessProbability) {
        LOG_DEBUG(
            fmt::format("failed to set state {} for axis {} (message was simulated to have gotten lost)", state, axis));
        states[axisID] = state;
      } else {
        LOG_DEBUG(fmt::format("successfully set state {} for axis {}", state, axis));
      }
      response[axisID].state = states[axisID];
    }
    server.respond(request, response);
  }

  void writeParameterCallback(Packet<WriteParametersRequest> const& request) {
    LOG_INFO("Process Write Parameter Request");
    LOG_DEBUG(fmt::format("Request:\n{}", request));
    WriteParametersResponse response;

    for (auto const& [axis, parameters] : request.payload) {
      auto& statuses = response[axis];  // calls the default constructor (creates an empty vector)
      for (auto const& [id, value] : parameters) {
        statuses.insert({id, ParameterStatus::SUCCESS});
      }
    }

    server.respond(request, response);
  }

  void readParameterCallback(Packet<ReadParametersRequest> const& request) {
    LOG_INFO("Process Read Parameter Request")
    LOG_DEBUG(fmt::format("Request:\n{}", request))
    ReadParametersResponse response;

    for (auto const& [axis, ids] : request.payload) {
      auto& parameters = response[axis];
      for (auto const& id : ids) {
        if (id == 0) {
        } else if (id == 1) {
        } else if (id == 2) {
          parameters.insert({id, 2.0});
        } else if (id == 3) {
          parameters.insert({id, 1});
        } else {
          parameters.insert({id, -1.0f});
        }
      }
    }

    server.respond(request, response);
  }

  void networkChangeCallback(Packet<NetworkChangeRequest> const& request) {
    NetworkChangeResponse response;
    NetworkData networkData;
    if (request.payload.find(ChangeNetworkIdentifier::NETWORK_INFO) != request.payload.end()) {
      networkData = request.payload.at(ChangeNetworkIdentifier::NETWORK_INFO);
      response[ChangeNetworkIdentifier::STATUS] = true;
    } else {
      networkData.ip = "127.0.0.1";
      networkData.mask = "255.255.255.0";
      networkData.mac = "aa:bb:cc:dd:ee:ff";
      response[ChangeNetworkIdentifier::STATUS] = false;
    }
    response[ChangeNetworkIdentifier::NETWORK_INFO] = networkData;

    server.respond(request, response);
  }
};
}  // namespace sv::robo
