
#include <iostream>

#include <gtest/gtest.h>

#include "ClientServerTest.h"

/**
 * @brief How to run this test:
 *  - Create a Run Configuration for the MockServer with Program arguments: "59629"
 *  - Create a gtest Run Configuration for the ClientExternalServerTestMain by
 *    - right-clicking on the ClientExternalServerTestMain.ccp
 *    - selecting "Create 'all in ClientExterna..."
 *    - for "Test/Kind" select "Suite", under "Suite" enter "ClientServerTest"
 *  - Modify the ip address MOCK_SERVER_IP in this file to be your own (or where the MockServer is running)
 *  - Run the mock server
 *  - Run the ClientExternalServerTestMain gtest
 *  - Profit
 */

namespace sv::robo {

static const std::string MOCK_SERVER_IP = "129.132.28.178";

TEST_F(ClientServerTest, ExternalServerSendReceiveTwoPackets) {
  unsigned short port = 59629;
  std::string address = MOCK_SERVER_IP;
  runSendReceiveTwoPackets(port, address);
}

TEST_F(ClientServerTest, ExternalServerStateMachineTest) {
  unsigned short port = 59629;
  std::string address = MOCK_SERVER_IP;
  runStateMachineTest(port, address);
}

}  // namespace sv::robo

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}
