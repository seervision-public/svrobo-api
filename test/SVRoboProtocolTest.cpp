#include <gtest/gtest.h>

#include "SVRoboProtocol.h"

namespace sv::robo {

TEST(SVRoboProtocolTest, StateTransitions) {
  // Check basic invalid transitions
  EXPECT_EQ(getStateTowards(AxisState::DISARMED, AxisState::DISARMED), std::nullopt);
  EXPECT_EQ(getStateTowards(AxisState::STOPPING, AxisState::STOPPING), std::nullopt);
  EXPECT_EQ(getStateTowards(AxisState::AUTO_CALIBRATION, AxisState::AUTO_CALIBRATION), std::nullopt);
  for (AxisState s : AxisState::_values()) {
    if (s != +AxisState::DISARMED) {
      EXPECT_EQ(getStateTowards(AxisState::DISARMED, s), std::nullopt);
    }
    if (s != +AxisState::DISARMED) {
      EXPECT_EQ(getStateTowards(s, AxisState::DISARMED), std::nullopt);
    }
    if (s != +AxisState::STOPPING) {
      EXPECT_EQ(getStateTowards(AxisState::STOPPING, s), std::nullopt);
    }
    if (s != +AxisState::AUTO_CALIBRATION) {
      EXPECT_EQ(getStateTowards(AxisState::AUTO_CALIBRATION, s), std::nullopt);
    }
  }
  // from DISCONNECTED
  EXPECT_EQ(getStateTowards(AxisState::DISCONNECTED, AxisState::DISCONNECTED), std::nullopt);
  EXPECT_EQ(getStateTowards(AxisState::DISCONNECTED, AxisState::DISABLED), StateAction::DISABLED);
  EXPECT_EQ(getStateTowards(AxisState::DISCONNECTED, AxisState::READY), StateAction::DISABLED);
  EXPECT_EQ(getStateTowards(AxisState::DISCONNECTED, AxisState::RUNNING), StateAction::DISABLED);
  EXPECT_EQ(getStateTowards(AxisState::DISCONNECTED, AxisState::STOPPING), std::nullopt);
  EXPECT_EQ(getStateTowards(AxisState::DISCONNECTED, AxisState::AUTO_CALIBRATION), StateAction::DISABLED);
  EXPECT_EQ(getStateTowards(AxisState::DISCONNECTED, AxisState::MANUAL_CALIBRATION), StateAction::DISABLED);

  // from DISABLED
  EXPECT_EQ(getStateTowards(AxisState::DISABLED, AxisState::DISCONNECTED), StateAction::DISCONNECTED);
  EXPECT_EQ(getStateTowards(AxisState::DISABLED, AxisState::DISABLED), std::nullopt);
  EXPECT_EQ(getStateTowards(AxisState::DISABLED, AxisState::READY), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::DISABLED, AxisState::RUNNING), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::DISABLED, AxisState::STOPPING), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::DISABLED, AxisState::AUTO_CALIBRATION), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::DISABLED, AxisState::MANUAL_CALIBRATION), StateAction::READY);
  // from READY
  EXPECT_EQ(getStateTowards(AxisState::READY, AxisState::DISCONNECTED), StateAction::DISABLED);
  EXPECT_EQ(getStateTowards(AxisState::READY, AxisState::DISABLED), StateAction::DISABLED);
  EXPECT_EQ(getStateTowards(AxisState::READY, AxisState::READY), std::nullopt);
  EXPECT_EQ(getStateTowards(AxisState::READY, AxisState::RUNNING), StateAction::RUNNING);
  EXPECT_EQ(getStateTowards(AxisState::READY, AxisState::STOPPING), StateAction::RUNNING);
  EXPECT_EQ(getStateTowards(AxisState::READY, AxisState::AUTO_CALIBRATION), StateAction::AUTO_CALIBRATION);
  EXPECT_EQ(getStateTowards(AxisState::READY, AxisState::MANUAL_CALIBRATION), StateAction::MANUAL_CALIBRATION);
  // from RUNNING
  EXPECT_EQ(getStateTowards(AxisState::RUNNING, AxisState::DISCONNECTED), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::RUNNING, AxisState::DISABLED), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::RUNNING, AxisState::READY), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::RUNNING, AxisState::RUNNING), std::nullopt);
  EXPECT_EQ(getStateTowards(AxisState::RUNNING, AxisState::STOPPING), StateAction::STOPPING);
  EXPECT_EQ(getStateTowards(AxisState::RUNNING, AxisState::AUTO_CALIBRATION), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::RUNNING, AxisState::MANUAL_CALIBRATION), StateAction::READY);
  // from MANUAL_CALIBRATION
  EXPECT_EQ(getStateTowards(AxisState::MANUAL_CALIBRATION, AxisState::DISCONNECTED), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::MANUAL_CALIBRATION, AxisState::DISABLED), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::MANUAL_CALIBRATION, AxisState::READY), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::MANUAL_CALIBRATION, AxisState::RUNNING), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::MANUAL_CALIBRATION, AxisState::STOPPING), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::MANUAL_CALIBRATION, AxisState::AUTO_CALIBRATION), StateAction::READY);
  EXPECT_EQ(getStateTowards(AxisState::MANUAL_CALIBRATION, AxisState::MANUAL_CALIBRATION), std::nullopt);
}

TEST(SVRoboProtocolTest, StateMachineResponseEqual) {
  StateMachineResponse response;
  response[PTUAxisID::pan].state = AxisState::STOPPING;
  response[PTUAxisID::tilt].state = AxisState::MANUAL_CALIBRATION;
  response[PTUAxisID::zoom].state = AxisState::READY;
  response[PTUAxisID::focus].state = AxisState::DISABLED;
  response[PTUAxisID::iris].state = AxisState::RUNNING;

  StateMachineRequest request;
  request[PTUAxisID::pan] = AxisState::STOPPING;
  request[PTUAxisID::tilt] = AxisState::MANUAL_CALIBRATION;
  request[PTUAxisID::zoom] = AxisState::READY;
  request[PTUAxisID::focus] = AxisState::DISABLED;
  request[PTUAxisID::iris] = AxisState::RUNNING;

  EXPECT_TRUE(checkResponse(response, request));
}

TEST(SVRoboProtocolTest, StateMachineResponseEqualNot) {
  StateMachineResponse response;
  response[PTUAxisID::pan].state = AxisState::DISCONNECTED;
  response[PTUAxisID::tilt].state = AxisState::STOPPING;
  response[PTUAxisID::zoom].state = AxisState::DISABLED;
  response[PTUAxisID::focus].state = AxisState::DISCONNECTED;
  response[PTUAxisID::iris].state = AxisState::READY;

  StateMachineRequest request;
  request[PTUAxisID::pan] = AxisState::READY;
  request[PTUAxisID::tilt] = AxisState::READY;
  request[PTUAxisID::zoom] = AxisState::READY;
  request[PTUAxisID::focus] = AxisState::READY;
  request[PTUAxisID::iris] = AxisState::READY;
  EXPECT_FALSE(checkResponse(response, request));
}

}  // namespace sv::robo
