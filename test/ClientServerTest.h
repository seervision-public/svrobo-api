#pragma once

#include <algorithm>
#include <chrono>
#include <cmath>
#include <thread>

#include <gtest/gtest.h>

#include "LogAPI.h"
#include "SVRoboCommunicator.h"
#include "SVRoboPacket.h"
#include "external_server/MockServer.h"

namespace sv::robo {

#if __cplusplus > 201402L  // This is only for C++17 (some catkin_make packages do not yet run with C++17, as it seems)
template<class... Ts>
struct overloaded : Ts... {
  using Ts::operator()...;
};
template<class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;
#endif

class ClientServerTest : public ::testing::Test {
 public:
  static constexpr std::chrono::seconds REQUEST_TIMEOUT{30};

  void runSendReceiveTwoPackets(const unsigned short& port, const std::string& address) {
    // Create packet
    constexpr uint32_t packetNumber = 100;
    Header header{10, packetNumber, PacketType::WRITE_PARAMETERS};
    WriteParametersRequest payload;
    payload[PTUAxisID::pan] = Parameters();
    payload[PTUAxisID::pan].insert({ids[0], 4});
    payload[PTUAxisID::pan].insert({ids[1], 40.8f});
    payload[PTUAxisID::pan].insert({ids[2], false});
    payload[PTUAxisID::pan].insert({ids[3], -3});
    payload[PTUAxisID::pan].insert({ids[4], -33.368617});
    RawPacket packet(header, payload);

    bool responseReceived = false;

    SVRoboClient client(
        address,
        port,
        [&](Packet<DiscoverResponse> const& p) { LOG_DEBUG("DiscoverResponse"); },
        [&](Packet<ReferencesResponse> const& p) { LOG_DEBUG("ReferencesResponse"); },
        [&](Packet<WriteParametersResponse> const& p) {
          verifyResult(p, packetNumber);
          responseReceived = true;
        },
        [&](Packet<ReadParametersResponse> const& p) { LOG_DEBUG("ReadParametersResponse"); },
        [&](Packet<StateMachineResponse> const& p) { LOG_DEBUG("StateMachineResponse"); },
        [&](Packet<FirmwareResponse> const& p) { LOG_DEBUG("FirmwareResponse"); },
        [&](Packet<NetworkChangeResponse> const& p) { LOG_DEBUG("NetworkChangeResponse"); },
        [](ErrorCode) {}

    );

    client.setAddressAndPort(address, port);

    // Send and receive response
    client.start();
    client.send(packet);
    auto timeStart = std::chrono::high_resolution_clock::now();
    while (!responseReceived && (std::chrono::high_resolution_clock::now() - timeStart < REQUEST_TIMEOUT)) {
    }  // Stupid way to wait for the package to arrive
    if (!responseReceived) {
      FAIL() << "Waiting for response, timeout of " << REQUEST_TIMEOUT.count() << "s exceeded.";
    }
    // Send another package
    responseReceived = false;
    client.send(packet);
    timeStart = std::chrono::high_resolution_clock::now();
    while (!responseReceived && (std::chrono::high_resolution_clock::now() - timeStart < REQUEST_TIMEOUT)) {
    }  // Stupid way to wait for the package to arrive
    if (!responseReceived) {
      FAIL() << "Waiting for response, timeout of " << REQUEST_TIMEOUT.count() << "s exceeded.";
    }
  }

  void runReadParameter(const unsigned short& port, const std::string& address) {
    // Create packet
    constexpr uint32_t packetNumber = 100;
    Header header{10, packetNumber, PacketType::READ_PARAMETERS};
    ReadParametersRequest payload;
    payload[PTUAxisID::pan];
    payload[PTUAxisID::pan].push_back(0);
    payload[PTUAxisID::pan].push_back(1);
    payload[PTUAxisID::pan].push_back(2);
    payload[PTUAxisID::pan].push_back(3);

    payload[PTUAxisID::tilt];
    payload[PTUAxisID::tilt].push_back(100);
    RawPacket packet(header, payload);

    bool responseReceived = false;

    SVRoboClient client(
        address,
        port,
        [&](Packet<DiscoverResponse> const& p) { LOG_DEBUG("DiscoverResponse"); },
        [&](Packet<ReferencesResponse> const& p) { LOG_DEBUG("ReferencesResponse"); },
        [&](Packet<WriteParametersResponse> const& p) { LOG_DEBUG("WriteParameterResponse"); },
        [&](Packet<ReadParametersResponse> const& p) {
          verifyResult(p, packetNumber);
          responseReceived = true;
        },
        [&](Packet<StateMachineResponse> const& p) { LOG_DEBUG("StateMachineResponse"); },
        [&](Packet<FirmwareResponse> const& p) { LOG_DEBUG("FirmwareResponse"); },
        [&](Packet<NetworkChangeResponse> const& p) { LOG_DEBUG("NetworkChangeResponse"); },
        [](ErrorCode) {}

    );

    client.setAddressAndPort(address, port);

    // Send and receive response
    client.start();
    client.send(packet);
    auto timeStart = std::chrono::high_resolution_clock::now();
    while (!responseReceived && (std::chrono::high_resolution_clock::now() - timeStart < REQUEST_TIMEOUT)) {
    }  // Stupid way to wait for the package to arrive
    if (!responseReceived) {
      FAIL() << "Waiting for response, timeout of " << REQUEST_TIMEOUT.count() << "s exceeded.";
    }
  }

  void verifyResult(Packet<WriteParametersResponse> const& response, uint16_t packetNumber) {
    ASSERT_EQ(static_cast<int>(response.header.type), static_cast<int>(PacketType::WRITE_PARAMETERS));
    ASSERT_EQ(response.header.number, packetNumber);
    ASSERT_EQ(response.payload.size(), 1);
    ASSERT_EQ(response.payload.at(PTUAxisID::pan).size(), 5);
    // for (auto id : ids) {
    //   EXPECT_TRUE(std::any_of(
    //       response.payload.at(PTUAxisID::pan).begin(), response.payload.at(PTUAxisID::pan).end(), [id](auto p) {
    //         return p.id == id && static_cast<uint8_t>(p.status) == static_cast<uint8_t>(ParameterStatus::SUCCESS);
    //       }));
    // }
  }
  void verifyResult(Packet<ReadParametersResponse> const& response, uint16_t packetNumber) {
    ASSERT_EQ(static_cast<int>(response.header.type), static_cast<int>(PacketType::READ_PARAMETERS));
    ASSERT_EQ(response.header.number, packetNumber);
    ASSERT_EQ(response.payload.size(), 2);
    ASSERT_EQ(response.payload.at(PTUAxisID::pan).size(), 2);
    ASSERT_EQ(response.payload.at(PTUAxisID::tilt).size(), 1);

    for (auto const& [axis, parameters] : response.payload) {
      for (auto const& [paramId, paramValue] : parameters) {
        std::visit(overloaded{[&](double value) {
                                EXPECT_DOUBLE_EQ(value, 2.0);
                                EXPECT_EQ(paramId, 2);
                              },
                              [&](float value) {
                                EXPECT_FLOAT_EQ(value, -1.0f);
                                EXPECT_EQ(paramId, 100);
                              },
                              [&](unsigned int value) {
                                EXPECT_EQ(value, 1);
                                EXPECT_EQ(paramId, 3);
                              },
                              [&](auto) {
                                if (paramId == 0) {
                                } else {
                                  EXPECT_EQ(paramId, 1);
                                }
                              }},
                   paramValue);
      }
    }
  }

  void runStateMachineTest(const unsigned short& port, const std::string& address) {
    static constexpr double timeout = 20.0;           // in seconds
    static constexpr double pollingFrequency = 20.0;  // in Hz

    bool responseReceived = false;
    std::mutex mutex;
    StateMachineResponse response;

    SVRoboClient client(
        address,
        port,
        [&](Packet<DiscoverResponse> const& p) { LOG_DEBUG("DiscoverResponse"); },
        [&](Packet<ReferencesResponse> const& p) { LOG_DEBUG("ReferencesResponse"); },
        [&](Packet<WriteParametersResponse> const& p) { LOG_DEBUG("WriteParameterResponse"); },
        [&](Packet<ReadParametersResponse> const& p) { LOG_DEBUG("ReadParametersResponse"); },
        [&](Packet<StateMachineResponse> const& p) {
          std::lock_guard lock(mutex);
          response = p.payload;
          responseReceived = true;
        },
        [&](Packet<FirmwareResponse> const& p) { LOG_DEBUG("FirmwareResponse"); },
        [&](Packet<NetworkChangeResponse> const& p) { LOG_DEBUG("NetworkChangeResponse"); },
        [](ErrorCode) {}

    );

    // Create packet requested and expected
    constexpr uint32_t packetNumber = 150;
    Header header{10, packetNumber, PacketType::STATE_MACHINE};
    StateMachineRequest payload;
    payload[PTUAxisID::pan] = AxisState::RUNNING;
    payload[PTUAxisID::tilt] = AxisState::DISARMED;
    payload[PTUAxisID::zoom] = AxisState::STOPPING;
    payload[PTUAxisID::focus] = AxisState::AUTO_CALIBRATION;
    payload[PTUAxisID::iris] = AxisState::READY;

    RawPacket packet(header, payload);

    // Send and receive response
    client.start();

    auto timeStart = std::chrono::high_resolution_clock::now();
    bool timedOut = true;
    while (std::chrono::duration<double>(std::chrono::high_resolution_clock::now() - timeStart).count() < timeout) {
      responseReceived = false;
      client.send(packet);
      auto timeStartResp = std::chrono::high_resolution_clock::now();
      while (!responseReceived && (std::chrono::high_resolution_clock::now() - timeStartResp < REQUEST_TIMEOUT)) {
      }  // Stupid way to wait for the package to arrive
      if (!responseReceived) {
        FAIL() << "Waiting for response, timeout of " << REQUEST_TIMEOUT.count() << "s exceeded.";
      }
      std::lock_guard lock(mutex);
      if (checkResponse(response, payload)) {
        timedOut = false;
        break;
      }
    }
    EXPECT_FALSE(timedOut);
  }

  void runDiscoveryTest(const unsigned short& port, const std::string& address) {
    static constexpr double timeout = 20.0;           // in seconds
    static constexpr double pollingFrequency = 20.0;  // in Hz

    bool responseReceived = false;
    uint16_t packetNumber = 150;
    uint32_t sessionId = 1;
    DiscoverResponse response;
    Header header{sessionId, packetNumber, PacketType::DISCOVER};

    // Create packet requested and expected
    DiscoverRequest payload;
    RawPacket packet(header, payload);

    SVRoboClient client(
        address,
        port,
        [&](Packet<DiscoverResponse> const& p) {
          verifyResult(p, packetNumber);
          responseReceived = true;
        },
        [&](Packet<ReferencesResponse> const& p) { LOG_DEBUG("ReferencesResponse"); },
        [&](Packet<WriteParametersResponse> const& p) { LOG_DEBUG("WriteParameterResponse"); },
        [&](Packet<ReadParametersResponse> const& p) { LOG_DEBUG("ReadParametersResponse"); },
        [&](Packet<StateMachineResponse> const& p) { LOG_DEBUG("StateMachineResponse"); },
        [&](Packet<FirmwareResponse> const& p) { LOG_DEBUG("FirmwareResponse"); },
        [&](Packet<NetworkChangeResponse> const& p) { LOG_DEBUG("NetworkChangeResponse"); },
        [](ErrorCode) {}

    );

    // Send and receive response
    client.setAddressAndPort(address, port);

    // Send and receive response
    client.start();
    client.send(packet);
    auto timeStart = std::chrono::high_resolution_clock::now();
    while (!responseReceived && (std::chrono::high_resolution_clock::now() - timeStart < REQUEST_TIMEOUT)) {
    }  // Stupid way to wait for the package to arrive
    if (!responseReceived) {
      FAIL() << "Waiting for response, timeout of " << REQUEST_TIMEOUT.count() << "s exceeded.";
    }
  }

  void verifyResult(Packet<DiscoverResponse> const& response, uint32_t packetNumber) {
    LOG_DEBUG(fmt::format("DiscoverResponse:\n{}", response));
    ASSERT_EQ(static_cast<int>(response.header.type), static_cast<int>(PacketType::DISCOVER));
    ASSERT_EQ(response.header.number, packetNumber);
    ASSERT_EQ(response.payload.version.major, 1);
    ASSERT_EQ(response.payload.version.minor, 7);
    ASSERT_EQ(response.payload.version.apiType, (+ApiType::NOMINAL)._to_integral());
    ASSERT_EQ(response.payload.network.size(), 2);
  }

  void runChangeNetwork(const unsigned short& port, const std::string& address) {
    // Create packet
    constexpr uint32_t packetNumber = 100;
    Header header{10, packetNumber, PacketType::NETWORK};
    NetworkChangeRequest payload;
    payload[ChangeNetworkIdentifier::NETWORK_INFO] = {address, "255.255.0.0", "zz:zz:zz:11:22:33"};
    RawPacket packet(header, payload);

    bool responseReceived = false;

    SVRoboClient client(
        address,
        port,
        [&](Packet<DiscoverResponse> const& p) { LOG_DEBUG("DiscoverResponse"); },
        [&](Packet<ReferencesResponse> const& p) { LOG_DEBUG("ReferencesResponse"); },
        [&](Packet<WriteParametersResponse> const& p) { LOG_DEBUG("WriteParameterResponse"); },
        [&](Packet<ReadParametersResponse> const& p) { LOG_DEBUG("NetworkChangeResponse") },
        [&](Packet<StateMachineResponse> const& p) { LOG_DEBUG("StateMachineResponse"); },
        [&](Packet<FirmwareResponse> const& p) { LOG_DEBUG("FirmwareResponse"); },
        [&](Packet<NetworkChangeResponse> const& p) {
          verifyResult(payload, p, packetNumber);
          responseReceived = true;
        },
        [](ErrorCode) {});

    // Send and receive response
    client.setAddressAndPort(address, port);

    // Send and receive response
    client.start();
    client.send(packet);
    auto timeStart = std::chrono::high_resolution_clock::now();
    while (!responseReceived && (std::chrono::high_resolution_clock::now() - timeStart < REQUEST_TIMEOUT)) {
    }  // Stupid way to wait for the package to arrive
    if (!responseReceived) {
      FAIL() << "Waiting for response, timeout of " << REQUEST_TIMEOUT.count() << "s exceeded.";
    }
  }

  void verifyResult(NetworkChangeRequest const& request,
                    Packet<NetworkChangeResponse> const& response,
                    uint32_t packetNumber) {
    ASSERT_EQ(static_cast<int>(response.header.type), static_cast<int>(PacketType::NETWORK));
    ASSERT_EQ(response.header.number, packetNumber);
    ASSERT_EQ(response.payload.size(), 2);

    for (auto const& [id, val] : response.payload) {
      std::visit(overloaded{[&](bool value) {
                              EXPECT_TRUE(value);
                              EXPECT_EQ(id, ChangeNetworkIdentifier::STATUS);
                            },
                            [&](NetworkData value) {
                              EXPECT_EQ(id, ChangeNetworkIdentifier::NETWORK_INFO);
                              EXPECT_STREQ(value.ip.c_str(), request.at(id).ip.c_str());
                              EXPECT_STREQ(value.mask.c_str(), request.at(id).mask.c_str());
                              EXPECT_STREQ(value.mac.c_str(), request.at(id).mac.c_str());
                            }},
                 val);
    }
  }

 private:
  std::vector<uint16_t> ids{1, 4, 7, 5, 52449};
};

}  // namespace sv::robo
