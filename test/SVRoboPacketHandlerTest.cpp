#include <gtest/gtest.h>
#include <msgpack.hpp>

#include "LogAPI.h"
#include "SVRoboPacket.h"
#include "SVRoboPacketHandler.h"

namespace sv::robo {

TEST(SVRoboPacketHandler, ReferenceRequestPacket) {
  constexpr uint32_t packetNumber = 10;
  constexpr uint32_t sessionId = 10;
  Header headerIn{sessionId, packetNumber, PacketType::REFERENCES};
  ReferencesRequest payload;
  payload[PTUAxisID::pan][ValueIdentifier::ANGULAR_VELOCITY] = 0.1;

  RawPacket packet(headerIn, payload);

  // EXPECT_EQ(packet.length(), 21);

  Packet<ReferencesRequest> extract;
  RequestPacketHandler requestHandler([](Packet<DiscoverRequest> const& p) {},
                                      [&](Packet<ReferencesRequest> const& p) { extract = p; },
                                      [](Packet<WriteParametersRequest> const& p) {},
                                      [](Packet<ReadParametersRequest> const& p) {},
                                      [](Packet<StateMachineRequest> const& p) {},
                                      [](Packet<FirmwareRequest> const& p) {},
                                      [](Packet<NetworkChangeRequest> const& p) {});

  requestHandler.receive(packet);
  Header const& header = extract.header;

  EXPECT_EQ(header.number, packetNumber);
  PacketType messageType = PacketType::REFERENCES;
  EXPECT_EQ(header.type, messageType);

  std::stringstream original;
  original << payload;
  std::stringstream unpacked;
  unpacked << extract.payload;

  EXPECT_EQ(original.str(), unpacked.str());
}

TEST(SVRoboPacketHandler, ReferenceResponsePacket) {
  constexpr uint32_t packetNumber = 10;
  constexpr uint32_t sessionId = 10;
  Header headerIn{sessionId, packetNumber, PacketType::REFERENCES};
  ReferencesResponse payload;
  payload[PTUAxisID::focus].status = ReferenceStatus::INVALID;
  payload[PTUAxisID::focus].data[ValueIdentifier::UNIT_POSITION] = 0.0f;

  RawPacket packet(headerIn, payload);
  EXPECT_EQ(packet.length(), 16);
  Packet<ReferencesResponse> extract;
  ResponsePacketHandler responseHandler([](Packet<DiscoverResponse> const& p) {},
                                        [&](Packet<ReferencesResponse> const& p) { extract = p; },
                                        [](Packet<WriteParametersResponse> const& p) {},
                                        [](Packet<ReadParametersResponse> const& p) {},
                                        [](Packet<StateMachineResponse> const& p) {},
                                        [](Packet<FirmwareResponse> const& p) {},
                                        [](Packet<NetworkChangeResponse> const& p) {});
  responseHandler.receive(packet);
  EXPECT_EQ(extract.header.number, packetNumber);
  PacketType messageType = PacketType::REFERENCES;
  EXPECT_EQ(extract.header.type, messageType);

  std::stringstream original;
  original << payload;
  std::stringstream unpacked;
  unpacked << extract.payload;

  EXPECT_EQ(original.str(), unpacked.str());
}

TEST(SVRoboPacketHandler, WriteParametersRequestPacket) {
  constexpr uint32_t packetNumber = 100;
  constexpr uint32_t sessionId = 1;
  Header headerIn{sessionId, packetNumber, PacketType::WRITE_PARAMETERS};
  WriteParametersRequest payload;
  payload[PTUAxisID::pan].insert({1, 4});
  payload[PTUAxisID::pan].insert({4, 40.8f});
  payload[PTUAxisID::pan].insert({7, false});
  payload[PTUAxisID::pan].insert({5, -3});
  payload[PTUAxisID::pan].insert({52449, -33.368617});

  RawPacket packet(headerIn, payload);
  EXPECT_EQ(packet.length(), 32);  // @todo [damian.frick] check if correct

  Packet<WriteParametersRequest> extract;
  RequestPacketHandler requestHandler([](Packet<DiscoverRequest> const& p) {},
                                      [](Packet<ReferencesRequest> const& p) {},
                                      [&](Packet<WriteParametersRequest> const& p) { extract = p; },
                                      [](Packet<ReadParametersRequest> const& p) {},
                                      [](Packet<StateMachineRequest> const& p) {},
                                      [](Packet<FirmwareRequest> const& p) {},
                                      [](Packet<NetworkChangeRequest> const& p) {});

  requestHandler.receive(packet);
  EXPECT_EQ(extract.header.number, packetNumber);
  PacketType messageType = PacketType::WRITE_PARAMETERS;
  EXPECT_EQ(extract.header.type, messageType);
  std::stringstream original;
  original << payload;
  std::stringstream unpacked;
  unpacked << extract.payload;

  EXPECT_EQ(original.str(), unpacked.str());
}

}  // namespace sv::robo
