#include <ifaddrs.h>
#include <iostream>

#include <gtest/gtest.h>

#include "ClientServerTest.h"

namespace sv::robo {

static std::string getIPAddress() {
  std::string ipAddress = "Unable to get IP Address";
  struct ifaddrs* interfaces = NULL;
  struct ifaddrs* temp_addr = NULL;
  int success = 0;
  // retrieve the current interfaces - returns 0 on success
  success = getifaddrs(&interfaces);
  if (success == 0) {
    // Loop through linked list of interfaces
    temp_addr = interfaces;
    while (temp_addr != NULL) {
      if (temp_addr->ifa_addr->sa_family == AF_INET) {
        // Check if interface is en0 which is the wifi connection on the iPhone
        if (strcmp(temp_addr->ifa_name, "enp")) {
          ipAddress = inet_ntoa(reinterpret_cast<struct sockaddr_in*>(temp_addr->ifa_addr)->sin_addr);
        }
      }
      temp_addr = temp_addr->ifa_next;
    }
  }
  // Free memory
  freeifaddrs(interfaces);
  return ipAddress;
}

TEST_F(ClientServerTest, StartStopServer) {
  unsigned short port = 59629;
  //  std::string address = "129.132.126.212";
  std::string address = getIPAddress();
  MockServer server(port);
  server.start();
  server.start();
  server.stop();
  server.stop();
}

TEST_F(ClientServerTest, SendReceiveTwoPackets) {
  unsigned short port = 8679;
  //  std::string address = "129.132.126.212";
  std::string address = getIPAddress();
  MockServer server(8700);
  server.setPort(port);  // Testing also setPort
  server.start();
  runSendReceiveTwoPackets(port, address);
}

TEST_F(ClientServerTest, StateMachineTest) {
  unsigned short port = 59629;
  //  std::string address = "10.10.12.198";
  std::string address = getIPAddress();
  MockServer server(port);
  server.start();
  runStateMachineTest(port, address);
}

TEST_F(ClientServerTest, ReadParameterTest) {
  unsigned short port = 59629;
  //  std::string address = "10.10.12.198";
  std::string address = getIPAddress();
  MockServer server(port);
  server.start();
  runReadParameter(port, address);
}
TEST_F(ClientServerTest, ChangeNetworkTest) {
  unsigned short port = 59629;
  //  std::string address = "10.10.12.198";
  std::string address = getIPAddress();
  MockServer server(port);
  server.start();
  runChangeNetwork(port, address);
}

TEST_F(ClientServerTest, DiscoverTest) {
  unsigned short port = 59629;
  //  std::string address = "10.10.12.198";
  std::string address = getIPAddress();
  MockServer server(port);
  server.start();
  runDiscoveryTest(port, address);
}

}  // namespace sv::robo
