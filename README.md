# SV Robo API

This repository contains a dependency free version of SV Robo API on both server and client.


To build:

```bash
- mkdir build
- cd build
- cmake ..
- make
```
